EESchema Schematic File Version 2
LIBS:KicadChessboard-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:DiodesInc
LIBS:Microchip
LIBS:myLib
LIBS:NXP
LIBS:KicadChessboard-cache
EELAYER 25 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 12 20
Title "Coil12"
Date "2016-12-24"
Rev "3.0"
Comp ""
Comment1 "Coil12 and matching and Q-adjust components."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6900 2950 6900 4350
Text GLabel 2800 4700 0    60   Input ~ 0
TX2/RX2
Text GLabel 2800 2600 0    60   Input ~ 0
TX1/RX1
Wire Wire Line
	2800 4700 3450 4700
Connection ~ 4400 2600
Connection ~ 4400 4700
Text GLabel 4100 3650 0    60   Output ~ 0
TVSS
Connection ~ 4400 3650
Connection ~ 5550 4700
Connection ~ 7300 4700
Wire Wire Line
	4400 4700 4400 4250
Wire Wire Line
	7300 4700 7300 4600
Wire Wire Line
	4400 3200 4400 4050
Connection ~ 5550 2600
Wire Wire Line
	4400 2600 4400 3000
Connection ~ 7300 2600
$Comp
L C_Small C62
U 1 1 586A0CD5
P 3550 4700
F 0 "C62" V 3300 4650 50  0000 L CNN
F 1 "50pF" V 3400 4600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3550 4700 50  0001 C CNN
F 3 "" H 3550 4700 50  0000 C CNN
	1    3550 4700
	0    1    1    0   
$EndComp
$Comp
L C_Small C61
U 1 1 586A0CCA
P 3550 2600
F 0 "C61" V 3750 2600 50  0000 L CNN
F 1 "50pF" V 3650 2550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3550 2600 50  0001 C CNN
F 3 "" H 3550 2600 50  0000 C CNN
	1    3550 2600
	0    1    1    0   
$EndComp
$Comp
L C_Small C64
U 1 1 586A0CB2
P 4400 4150
F 0 "C64" H 4410 4220 50  0000 L CNN
F 1 "162pF" H 4410 4070 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4400 4150 50  0001 C CNN
F 3 "" H 4400 4150 50  0000 C CNN
	1    4400 4150
	1    0    0    -1  
$EndComp
$Comp
L C_Small C63
U 1 1 586A0D67
P 4400 3100
F 0 "C63" H 4410 3170 50  0000 L CNN
F 1 "162pF" H 4410 3020 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4400 3100 50  0001 C CNN
F 3 "" H 4400 3100 50  0000 C CNN
	1    4400 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2600 7300 2700
Wire Wire Line
	8800 2600 8800 3350
Connection ~ 7300 3650
Wire Wire Line
	7300 4100 7300 4200
Wire Wire Line
	7300 3600 7300 3700
Connection ~ 6900 3350
Wire Wire Line
	7000 3350 6900 3350
Connection ~ 6900 3950
Wire Wire Line
	6650 3950 7000 3950
Wire Wire Line
	6900 4350 7000 4350
Wire Wire Line
	7000 2950 6900 2950
Wire Wire Line
	7300 3100 7300 3200
$Comp
L BS107-RESCUE-KicadChessboard Q40
U 1 1 586A0D3A
P 7200 4400
F 0 "Q40" H 7400 4475 50  0000 L CNN
F 1 "DMN2400UFB" H 7400 4400 50  0001 L CNN
F 2 "DiodesInc:DMN2400UFB-7" H 7400 4325 50  0001 L CIN
F 3 "" H 7200 4400 50  0000 L CNN
	1    7200 4400
	1    0    0    1   
$EndComp
$Comp
L BS107-RESCUE-KicadChessboard Q39
U 1 1 586A0D28
P 7200 3900
F 0 "Q39" H 7400 3975 50  0000 L CNN
F 1 "DMN2400UFB" H 7400 3900 50  0001 L CNN
F 2 "DiodesInc:DMN2400UFB-7" H 7400 3825 50  0001 L CIN
F 3 "" H 7200 3900 50  0000 L CNN
	1    7200 3900
	1    0    0    -1  
$EndComp
$Comp
L BS107-RESCUE-KicadChessboard Q38
U 1 1 586A0D0F
P 7200 3400
F 0 "Q38" H 7400 3475 50  0000 L CNN
F 1 "DMN2400UFB" H 7400 3400 50  0001 L CNN
F 2 "DiodesInc:DMN2400UFB-7" H 7400 3325 50  0001 L CIN
F 3 "" H 7200 3400 50  0000 L CNN
	1    7200 3400
	1    0    0    1   
$EndComp
$Comp
L BS107-RESCUE-KicadChessboard Q37
U 1 1 586A0D0A
P 7200 2900
F 0 "Q37" H 7400 2975 50  0000 L CNN
F 1 "DMN2400UFB" H 7400 2900 50  0001 L CNN
F 2 "DiodesInc:DMN2400UFB-7" H 7400 2825 50  0001 L CIN
F 3 "" H 7200 2900 50  0000 L CNN
	1    7200 2900
	1    0    0    -1  
$EndComp
$Comp
L R_Small R47
U 1 1 586A0CF1
P 8150 2600
F 0 "R47" V 7950 2550 50  0000 L CNN
F 1 "2R2" V 8050 2550 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 8150 2600 50  0001 C CNN
F 3 "" H 8150 2600 50  0000 C CNN
	1    8150 2600
	0    1    1    0   
$EndComp
$Comp
L R_Small R48
U 1 1 586A0CA2
P 8150 4700
F 0 "R48" V 8350 4650 50  0000 L CNN
F 1 "2R2" V 8250 4650 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 8150 4700 50  0001 C CNN
F 3 "" H 8150 4700 50  0000 C CNN
	1    8150 4700
	0    1    1    0   
$EndComp
$Comp
L NFC_Differential_Coil L13
U 1 1 586A0CBA
P 8800 3650
F 0 "L13" H 9000 3700 50  0000 C CNN
F 1 "Coil_12" H 9000 3550 50  0000 C CNN
F 2 "libraries:NFC_4cm_4_coil_out" H 8800 3650 50  0001 C CNN
F 3 "" H 8800 3650 50  0000 C CNN
	1    8800 3650
	1    0    0    -1  
$EndComp
$Comp
L CTRIM C65
U 1 1 586A0CE9
P 5550 3100
F 0 "C65" H 5750 3150 50  0000 C CNN
F 1 "25pF" H 5750 3050 50  0000 C CNN
F 2 "libraries:Murata_TZY2_3.2x2.5" H 5550 3100 50  0001 C CNN
F 3 "" H 5550 3100 50  0000 C CNN
	1    5550 3100
	1    0    0    -1  
$EndComp
$Comp
L R_Small R45
U 1 1 58605A59
P 6450 2600
F 0 "R45" V 6650 2550 50  0000 L CNN
F 1 "0R" V 6550 2550 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 6450 2600 50  0001 C CNN
F 3 "" H 6450 2600 50  0000 C CNN
	1    6450 2600
	0    1    1    0   
$EndComp
$Comp
L R_Small R46
U 1 1 586A0D81
P 6450 4700
F 0 "R46" V 6650 4650 50  0000 L CNN
F 1 "0R" V 6550 4650 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 6450 4700 50  0001 C CNN
F 3 "" H 6450 4700 50  0000 C CNN
	1    6450 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 4700 5550 3250
Wire Wire Line
	5550 2600 5550 2950
Wire Wire Line
	4100 3650 8700 3650
Wire Wire Line
	6550 2600 8050 2600
Wire Wire Line
	3650 2600 6350 2600
Wire Wire Line
	3650 4700 6350 4700
Wire Wire Line
	6550 4700 8050 4700
Wire Wire Line
	8800 4700 8250 4700
Wire Wire Line
	8800 3950 8800 4700
Wire Wire Line
	8800 2600 8250 2600
Wire Wire Line
	2800 2600 3450 2600
Text GLabel 6650 3950 0    60   Input ~ 0
/COIL_EN_12
Text Notes 2900 2500 0    60   ~ 0
*in parallel with EMC caps...\nkeep small
Text Notes 2900 4950 0    60   ~ 0
*in parallel with EMC caps...\nkeep small
Text Notes 6200 2500 0    60   ~ 0
*Icoil measure
Wire Notes Line
	2850 2150 5850 2150
Wire Notes Line
	5850 2150 5850 5150
Wire Notes Line
	5850 5150 2850 5150
Wire Notes Line
	2850 5150 2850 2150
Text Notes 3000 2150 0    60   ~ 0
Matching
Text Notes 6200 4600 0    60   ~ 0
*Icoil measure
$EndSCHEMATC
