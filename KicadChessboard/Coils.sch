EESchema Schematic File Version 2
LIBS:KicadChessboard-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:DiodesInc
LIBS:Microchip
LIBS:myLib
LIBS:NXP
LIBS:KicadChessboard-cache
EELAYER 25 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 2 20
Title "Holder Sheet"
Date "2016-12-27"
Rev "3.0"
Comp ""
Comment1 "Holder sheet to contain 16 coil sheets."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3250 1500 750  600 
U 585E048E
F0 "Coil1" 60
F1 "Coil1.sch" 60
$EndSheet
$Sheet
S 4600 1500 750  600 
U 5869FA7D
F0 "Coil2" 60
F1 "Coil2.sch" 60
$EndSheet
$Sheet
S 7350 1500 750  600 
U 5869FC46
F0 "Coil4" 60
F1 "Coil4.sch" 60
$EndSheet
$Sheet
S 3250 2450 750  600 
U 5869FE90
F0 "Coil5" 60
F1 "Coil5.sch" 60
$EndSheet
$Sheet
S 4600 2450 750  600 
U 5869FE92
F0 "Coil6" 60
F1 "Coil6.sch" 60
$EndSheet
$Sheet
S 6000 2450 750  600 
U 5869FE94
F0 "Coil7" 60
F1 "Coil7.sch" 60
$EndSheet
$Sheet
S 3250 3400 750  600 
U 5869FFBC
F0 "Coil9" 60
F1 "Coil9.sch" 60
$EndSheet
$Sheet
S 4600 3400 750  600 
U 5869FFBE
F0 "Coil10" 60
F1 "Coil10.sch" 60
$EndSheet
$Sheet
S 6000 3400 750  600 
U 5869FFC0
F0 "Coil11" 60
F1 "Coil11.sch" 60
$EndSheet
$Sheet
S 7350 3400 750  600 
U 5869FFC2
F0 "Coil12" 60
F1 "Coil12.sch" 60
$EndSheet
$Sheet
S 3250 4350 750  600 
U 5869FFC4
F0 "Coil13" 60
F1 "Coil13.sch" 60
$EndSheet
$Sheet
S 4600 4350 750  600 
U 5869FFC6
F0 "Coil14" 60
F1 "Coil14.sch" 60
$EndSheet
$Sheet
S 6000 4350 750  600 
U 5869FFC8
F0 "Coil15" 60
F1 "Coil15.sch" 60
$EndSheet
$Sheet
S 7350 4350 750  600 
U 5869FFCA
F0 "Coil16" 60
F1 "Coil16.sch" 60
$EndSheet
$Sheet
S 7350 2450 750  600 
U 5869FE96
F0 "Coil8" 60
F1 "Coil8.sch" 60
$EndSheet
$Sheet
S 6000 1500 750  600 
U 5869FC44
F0 "Coil3" 60
F1 "Coil3.sch" 60
$EndSheet
$EndSCHEMATC
