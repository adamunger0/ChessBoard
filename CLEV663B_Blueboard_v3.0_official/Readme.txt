/*****************************************************************************
* Readme.txt: CLEV663B Blueboard design files
*
* Copyright(C) 2014, NXP Semiconductors
* All rights reserved.
*
* Revision History
* 2014.09.05 	V1.00 first packed version
*
******************************************************************************/

1. Document purpose

This document describes the content of the CLEV663B-V3.0 Board design files package

2. Content

-CLEV663B_Blueboard_3v0.SchDoc		Altium Designer Schematics file
-CLEV663B_Blueboard_3v0.PcbDoc		Altium Designer PCB-Layout file
-CLEV663B_Blueboard_3v0.PrjPCB		Altium Designer project file
-CLEV663B_Blueboard_3v0_BOM.xlsx		Bill of Material
-CLEV663B_Blueboard_3v0_SCH.PDF	Schematics PDF file
-Gerber					Altium Designer generated Gerber files
-Pick Place				Altium Designer generated Pick and Place files
-Readme.txt				this document

3. Mandatory material, not included

* Altium Designer with valid license: 
	http://www.altium.com/