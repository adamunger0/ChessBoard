/* 
 * File:   MFRC630.c
 * Author: Adam Unger
 * Description: This file implements functionality to interact with the MFRC630.
 *  Much of it's contents come from here: https://github.com/iwanders/MFRC630
 */

#include "MFRC630.h"
#include "Chessboard.h" // bitmasks and convenience functions
#include "I2C.h" // low level drivers for I2C interaction with MFRC630

// I2C address = 41 = 0x29 = 0b0101001, last bit is R/W
#define MFRC630_ADDR 0b01010010 


// ---------------------------------------------------------------------------
// Register interaction functions.
// ---------------------------------------------------------------------------

/*
 * Reads a register.
 * INPUT: const uint8_t reg - Specifies which register to read.
 * OUTPUT: uint8_t - the value of the register to be read.
 */
uint8_t MFRC630ReadReg(const uint8_t reg) {
    char data; // return value - data received from MFRC630
    
    // begin I2C transmission with start condition
    i2cStart(MSSP1);
    // write MFRC630 control byte - write
    i2cWrite(MSSP1, MFRC630_ADDR | I2C_WRITE_CMD);
    // write register location to read
    i2cWrite(MSSP1, reg);
    // send rep-start for data read
    i2cRepStart(MSSP1);
    // write MFRC630 control byte - read
    i2cWrite(MSSP1, MFRC630_ADDR | I2C_READ_CMD);
    // get data
    data = i2cRead(MSSP1);
    // master send NACK to slave
    i2cMasterAck(MSSP1, I2C_DATA_NOACK);
    // send stop
    i2cStop(MSSP1);
    
    return data;
}

/*
 * Write a register. Sets a single register to the provided value.
 * INPUT: const uint8_t data - Specifies the value to write to that register.
 *        const uint8_t reg - Specifies which register to write.
 * OUTPUT: none
 */
void MFRC630WriteReg(const uint8_t data, const uint8_t reg) {
    // begin I2C transmission with start condition
    i2cStart(MSSP1);
    // write MFRC630 control byte - write
    i2cWrite(MSSP1, MFRC630_ADDR | I2C_WRITE_CMD);    
    // write register address
    i2cWrite(MSSP1, reg);    
    // write data for register
    i2cWrite(MSSP1, data);
    // stop this interaction
    i2cStop(MSSP1);
}

/*
 * Write multiple registers. Sets consecutive registers to the provided values. 
 * INPUT: const uint8_t* data - An array of the values to write to the register
 *          starting from `reg`. The first value (`values[0]`) gets written to
 *          `reg`, the second (`values[1]`) to `reg+1`, and so on.
 *        const uint8_t reg - Specifies at which register writing starts.        
 *        const uint16_t len - The number of register to write.
 * OUTPUT: none
 */
void MFRC630WriteRegs(const uint8_t* data, const uint8_t reg, const uint16_t len) {
    // begin I2C transmission with start condition
    i2cStart(MSSP1);
    
    // Since the register pointer doesn't automatically increment it's necessary
    // to send the rep starts/control/address bytes for each write cycle
    for (uint16_t i = 0; i < len; ++i) {
        // write MFRC630 control byte - write
        i2cWrite(MSSP1, MFRC630_ADDR | I2C_WRITE_CMD);    
        // write register address
        i2cWrite(MSSP1, reg + i);    
        // write data for register
        i2cWrite(MSSP1, data[i]);
        // don't rep start on last piece of data
        if (i < len - 1) i2cRepStart(MSSP1);
    }
    
    // stop this interaction
    i2cStop(MSSP1);
}

/*
 * Write data to FIFO. The FIFO is located at register `#MFRC630_REG_FIFODATA`. 
 * Writes to this register do not automatically increment the write pointer in
 * the chip and multiple bytes may be written to this register to place them
 * into the FIFO buffer. This function does not clear the FIFO beforehand, it
 * only provides the raw transfer functionality. 
 * INPUT: const uint8_t* data - The data to be written into the FIFO.
 *        const uint16_t len - The number of bytes to be written into the FIFO.
 * OUTPUT: none
 */
void MFRC630WriteFIFO(const uint8_t* data, const uint16_t len) {
    // begin I2C transmission with start condition
    i2cStart(MSSP1);
    
    // write MFRC630 control byte - write
    i2cWrite(MSSP1, MFRC630_ADDR | I2C_WRITE_CMD);    
    // write register address
    i2cWrite(MSSP1, MFRC630_REG_FIFODATA);    
    
    for (uint16_t i = 0; i < len; ++i) {       
        // write data for FIFO
        i2cWrite(MSSP1, data[i]);
        // NOTE: would check ACK/NACK from slave here but we don't actually care
        // since there's nothing we could really do about it anyway.
    }
    
    // stop this interaction
    i2cStop(MSSP1);
}

/*
 * Read data from FIFO. This function reads data from the FIFO into an array on
 * the microcontroller. NOTE: This reads regardless of #MFRC630_REG_FIFOLENGTH,
 * if there aren't enough bytes present in the FIFO, they are read from the chip
 * anyway, these bytes should not be used. (The returned bytes from an empty 
 * FIFO are often identical to the last valid byte that was read from it.)
 * INPUT: uint8_t* data - The data read from the FIFO is placed into this array.
 *        const uint16_t len - The number of bytes to be read from the FIFO.
 * OUTPUT: none
 */
void MFRC630ReadFIFO(uint8_t* data, const uint16_t len) {  
    // begin I2C transmission with start condition
    i2cStart(MSSP1);
    
    // write MFRC630 control byte - write
    i2cWrite(MSSP1, MFRC630_ADDR | I2C_WRITE_CMD);
    // write register location to read
    i2cWrite(MSSP1, MFRC630_REG_FIFODATA);
    // send rep-start for data read
    i2cRepStart(MSSP1);
    // write MFRC630 control byte - read
    i2cWrite(MSSP1, MFRC630_ADDR | I2C_READ_CMD);
    
    for (uint16_t i = 0; i < len; ++i) {         
        // get data
        data[i] = i2cRead(MSSP1);
        if (i < len - 1) i2cMasterAck(MSSP1, I2C_DATA_ACK);
    }
    
    // send stop
    i2cStop(MSSP1);
}


// ---------------------------------------------------------------------------
// Command functions
//   These activate the various commands of the chip with the right arguments.
//   The chip has various commands it can execute, these commands often have
//   arguments which should be transferred to the FIFO before the command is
//   initiated. These functions provide this functionality of transferring the
//   arguments to the FIFO and then initiating the commands.
// ---------------------------------------------------------------------------


/*
 * Read data from EEPROM into the FIFO buffer. This instruction transfers data
 * from the EEPROM (section 2) at the given address location into the FIFO
 * buffer.
 * INPUT: const uint16_t address - The start address in the EEPROM to start
 *        reading from.
 *        const uint16_t length - The number of bytes to read from the EEPROM
 *        into the FIFO buffer.
 * OUTPUT: none (read FIFO for data)
 */
void MFRC630Cmd_ReadE2(const uint16_t address, const uint16_t length) {
    uint8_t parameters[3]; // for the read command parameters
    parameters[0] = (uint8_t) (address >> 8); // high 8-bits of EEPROM start address
    parameters[1] = (uint8_t) (address & 0xFF); // low 8-bits of EEPROM start address
    parameters[2] = length; // number of bytes to read from EEPROM
    MFRC630FlushFIFO(); // clear the FIFO for this write/read
    MFRC630WriteFIFO(parameters, 3); // write the parameters of this EEPROM read
    MFRC630WriteReg(MFRC630_CMD_READE2, MFRC630_REG_COMMAND); // send read command
}

/*
 * Write a byte to the EEPROM. This instruction transfers a single byte into the
 * EEPROM at the specified address.
 * INPUT: const uint16_t address - The address in the EEPROM to write to.
 *        const uint8_t data - The data which is destined for the EEPROM.
 * OUTPUT: none
 */
void MFRC630Cmd_WriteE2(const uint16_t address, const uint8_t data) {
    uint8_t parameters[3]; // for the write command parameters
    parameters[0] = (uint8_t) (address >> 8); // high 8-bits of EEPROM address
    parameters[1] = (uint8_t) (address & 0xFF); // low 8-bits of EEPROM address
    parameters[2] = data; // data to write to the EEPROM
    MFRC630FlushFIFO(); // clear the FIFO for this write/read
    MFRC630WriteFIFO(parameters, 3); // write the parameters of this EEPROM write    
    MFRC630WriteReg(MFRC630_CMD_WRITEE2, MFRC630_REG_COMMAND); // send read command
}

void MFRC630Cmd_LoadReg(const uint16_t address, const uint8_t regaddr, const uint16_t length) {
    uint8_t parameters[4];
    parameters[0] = (uint8_t) (address >> 8);
    parameters[1] = (uint8_t) (address & 0xFF);
    parameters[2] = regaddr;
    parameters[3] = length;
    MFRC630FlushFIFO();
    MFRC630WriteFIFO(parameters, 4);
    MFRC630WriteReg(MFRC630_CMD_LOADREG, MFRC630_REG_COMMAND);
}

/*
 * Load protocol settings. Loads register settings for the protocol indicated.
 * Can configure different protocols for rx and tx. The most common protocol is
 * MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER which is the default protocol
 * for the SELECT procedure. The most common protocols are listed in the
 * datasheet, but the MFRC630 Quickstart Guide AN11022 gives a complete 
 * description. 
 * INPUT: const uint8_t rx - The protocol number to load for the receiving
 *         frontend.
 *        const uint8_t tx - The protocol number to load for the transmitting
 *          frontend.
 * OUTPUT: none
 */
void MFRC630Cmd_LoadProtocol(const uint8_t rx, const uint8_t tx) {
    uint8_t parameters[2];
    parameters[0] = rx;
    parameters[1] = tx;
    MFRC630FlushFIFO();
    MFRC630WriteFIFO(parameters, 2);
    MFRC630WriteReg(MFRC630_CMD_LOADPROTOCOL, MFRC630_REG_COMMAND);
}

/*
 * Turn on the receive circuit by issuing the MFRC630_CMD_RECEIVE command.
 * INPUT: none
 * OUTPUT: data in FIFO buffer
 */
void MFRC630Cmd_Receive() {
    MFRC630Cmd_Idle();
    MFRC630FlushFIFO();
    MFRC630WriteReg(MFRC630_CMD_RECEIVE, MFRC630_REG_COMMAND);
}

/*
 * Transmit the bytes provided and go into receive mode. This function loads the
 * data from the `data` array into the FIFO, and then issues the
 * `MFRC630_CMD_TRANSCEIVE` command, which sends the data in the FIFO and
 * switches to receiving mode afterwards.
 * INPUT: const uint8_t* data - The data to be transmitted.
 *      const uint16_t len - The number of bytes to be read from `data` and be
 *       transmitted.
 * OUTPUT: data in FIFO buffer
 */
void MFRC630Cmd_Transceive(const uint8_t* data, const uint16_t len) {
    MFRC630Cmd_Idle();
    MFRC630FlushFIFO();
    MFRC630WriteFIFO(data, len);
    MFRC630WriteReg(MFRC630_CMD_TRANSCEIVE, MFRC630_REG_COMMAND);
}

void MFRC630Cmd_Idle() {
    MFRC630WriteReg(MFRC630_CMD_IDLE, MFRC630_REG_COMMAND);
}

void MFRC630Cmd_LoadKeyE2(const uint8_t key_nr) {
    MFRC630FlushFIFO();
    MFRC630WriteReg(key_nr, MFRC630_REG_FIFODATA);
    MFRC630WriteReg(MFRC630_CMD_LOADKEYE2, MFRC630_REG_COMMAND);
}

void MFRC630Cmd_Auth(const uint8_t key_type, const uint8_t block_address, const uint8_t* card_uid) {
    MFRC630Cmd_Idle();
    uint8_t parameters[6];
    parameters[0] = key_type;
    parameters[1] = block_address;
    parameters[2] = card_uid[0];
    parameters[3] = card_uid[1];
    parameters[4] = card_uid[2];
    parameters[5] = card_uid[3];
    MFRC630FlushFIFO();
    MFRC630WriteFIFO(parameters, 6);
    MFRC630WriteReg(MFRC630_CMD_MFAUTHENT, MFRC630_REG_COMMAND);
}

void MFRC630Cmd_LoadKey(const uint8_t* key) {
    MFRC630Cmd_Idle();
    MFRC630FlushFIFO();
    MFRC630WriteFIFO(key, 6);
    MFRC630WriteReg(MFRC630_CMD_LOADKEY, MFRC630_REG_COMMAND);
}

// ---------------------------------------------------------------------------
// Utility functions.
// ---------------------------------------------------------------------------

void MFRC630FlushFIFO() {
    MFRC630WriteReg(1<<4, MFRC630_REG_FIFOCONTROL);
}

uint16_t MFRC630FIFOLength() {
    // should do 512 byte fifo handling here.
    return MFRC630ReadReg(MFRC630_REG_FIFOLENGTH);
}

void MFRC630ClearIRQ0() {
    MFRC630WriteReg((uint8_t) ~(1<<7), MFRC630_REG_IRQ0);
}
void MFRC630ClearIRQ1() {
    MFRC630WriteReg((uint8_t) ~(1<<7), MFRC630_REG_IRQ1);
}

uint8_t MFRC630_IRQ0() {
    return MFRC630ReadReg(MFRC630_REG_IRQ0);
}
uint8_t MFRC630_IRQ1() {
    return MFRC630ReadReg(MFRC630_REG_IRQ1);
}

uint8_t MFRC630TransferE2Page(uint8_t* dest, const uint8_t page) {
    MFRC630Cmd_ReadE2(page * 64, 64);
    uint8_t res = MFRC630FIFOLength();
    MFRC630ReadFIFO(dest, 64);
    return res;
}

// ---------------------------------------------------------------------------
// Timer functions
// ---------------------------------------------------------------------------
void MFRC630ActivateTimer(const uint8_t timer, const uint8_t active) {
    MFRC630WriteReg(((active << timer) << 4) | (1 << timer), MFRC630_REG_TCONTROL);
}

void MFRC630TimerSetControl(const uint8_t timer, const uint8_t value) {
    MFRC630WriteReg(value, MFRC630_REG_T0CONTROL + (5 * timer));
}

void MFRC630TimerSetReload(const uint8_t timer, const uint16_t value) {
    MFRC630WriteReg(value >> 8, MFRC630_REG_T0RELOADHI + (5 * timer));
    MFRC630WriteReg(0xFF, MFRC630_REG_T0RELOADLO + (5 * timer));
}

void MFRC630TimerSetValue(const uint8_t timer, const uint16_t value) {
    MFRC630WriteReg(value >> 8, MFRC630_REG_T0COUNTERVALHI + (5 * timer));
    MFRC630WriteReg(0xFF, MFRC630_REG_T0COUNTERVALLO + (5 * timer));
}

uint16_t MFRC630TimerGetValue(const uint8_t timer) {
    uint16_t res = MFRC630ReadReg(MFRC630_REG_T0COUNTERVALHI + (5 * timer)) << 8;
    res += MFRC630ReadReg(MFRC630_REG_T0COUNTERVALLO + (5 * timer));
    return res;
}

/*
 * Start IQ Measurement. From Application Note 11145, section 3.2.1, it
 * configures the registers to perform an IQ measurement.
 * INPUT: none
 * OUTPUT: none
 */
void MFRC630_AN11145_StartIQMeasurement() {
    // Part-1, configurate LPCD Mode
    // Please remove any PICC from the HF of the reader.
    // "I" and the "Q" values read from reg 0x42 and 0x43
    // shall be used in part-2 "Detect PICC"
    //  reset CLRC663 and idle
    MFRC630WriteReg(0x1F, 0);
    // Should sleep here... for 50ms... can do without.
    MFRC630WriteReg(0, 0);
    // disable IRQ0, IRQ1 interrupt sources
    MFRC630WriteReg(0x7F, 0x06);
    MFRC630WriteReg(0x7F, 0x07);
    MFRC630WriteReg(0x00, 0x08);
    MFRC630WriteReg(0x00, 0x09);
    MFRC630WriteReg(0xB0, 0x02);  // Flush FIFO
    // LPCD_config
    MFRC630WriteReg(0xC0, 0x3F);  // Set Qmin register
    MFRC630WriteReg(0xFF, 0x40);  // Set Qmax register
    MFRC630WriteReg(0xC0, 0x41);  // Set Imin register
    MFRC630WriteReg(0x89, 0x28);  // set DrvMode register
    // Execute trimming procedure
    MFRC630WriteReg(0x00, 0x1F);  // Write default. T3 reload value Hi
    MFRC630WriteReg(0x10, 0x20);  // Write default. T3 reload value Lo
    MFRC630WriteReg(0x00, 0x24);  // Write min. T4 reload value Hi
    MFRC630WriteReg(0x05, 0x25);  // Write min. T4 reload value Lo
    MFRC630WriteReg(0xF8, 0x23);  // Config T4 for AutoLPCD&AutoRestart.Set AutoTrimm bit.Start T4.
    MFRC630WriteReg(0x40, 0x43);  // Clear LPCD result
    MFRC630WriteReg(0x52, 0x38);  // Set Rx_ADCmode bit
    MFRC630WriteReg(0x03, 0x39);  // Raise receiver gain to maximum
    MFRC630WriteReg(0x01, 0x00);  // Execute Rc663 command "Auto_T4" (Low power card detection and/or Auto trimming)
}

void mfrc630_AN11145_stop_IQ_measurement() {
    // Flush cmd and Fifo
    MFRC630WriteReg(0x00, 0x00);
    MFRC630WriteReg(0xB0, 0x02);
    MFRC630WriteReg(0x12, 0x38);  // Clear Rx_ADCmode bit
    //> ------------ I and Q Value for LPCD ----------------
    // mfrc630_read_reg(MFRC630_REG_LPCD_I_RESULT) & 0x3F
    // mfrc630_read_reg(MFRC630_REG_LPCD_Q_RESULT) & 0x3F
}

/*
 * Set the registers to the recommended values, skipping the first skip
 * registers. Sets the recommended registers but allows for an arbitrary number
 * of registers to be skipped at the start. 
 * INPUT: const uint8_t protocol - The protocol index to set the registers to.
 *         Only MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER, 
 *         MFRC630_PROTO_ISO14443A_212_MILLER_BPSK,
 *         MFRC630_PROTO_ISO14443A_424_MILLER_BPSK and
 *         MFRC630_PROTO_ISO14443A_848_MILLER_BPSK were copied. The recommended
 *         values for the other protocols can be found in the application note.
 *        const uint8_t skip - The number of registers to skip from the start.
 */
void MFRC630_AN1102_RecommendedRegistersSkip(const uint8_t protocol, const uint8_t skip) {
    switch (protocol) {
        case MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER: {
                const uint8_t buf[] = MFRC630_RECOM_14443A_ID1_106;
                MFRC630WriteRegs(buf+skip, MFRC630_REG_DRVMOD+skip, sizeof(buf)-skip);
            }
            break;
        case MFRC630_PROTO_ISO14443A_212_MILLER_BPSK: {
                const uint8_t buf[] = MFRC630_RECOM_14443A_ID1_212;
                MFRC630WriteRegs(buf+skip, MFRC630_REG_DRVMOD+skip, sizeof(buf)-skip);
            }
            break;
        case MFRC630_PROTO_ISO14443A_424_MILLER_BPSK: {
                const uint8_t buf[] = MFRC630_RECOM_14443A_ID1_424;
                MFRC630WriteRegs(buf+skip, MFRC630_REG_DRVMOD+skip, sizeof(buf)-skip);
            }
            break;
        case MFRC630_PROTO_ISO14443A_848_MILLER_BPSK: {
                const uint8_t buf[] = MFRC630_RECOM_14443A_ID1_848;
                MFRC630WriteRegs(buf+skip, MFRC630_REG_DRVMOD+skip, sizeof(buf)-skip);
            }
            break;
    }
}

/*
 * Set the registers to the recommended values. This function uses the
 * recommended registers from the datasheets, it should yield identical results
 * to the  MFRC630Cmd_LoadProtocol() function.
 * INPUT: const uint8_t protocol - The protocol index to set the registers to.
 *         Only MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER,
 *         MFRC630_PROTO_ISO14443A_212_MILLER_BPSK,
 *         MFRC630_PROTO_ISO14443A_424_MILLER_BPSK and 
 *         MFRC630_PROTO_ISO14443A_848_MILLER_BPSK were copied. The recommended
 *         values for the other protocols can be found in the application note.
 * OUTPUT: none
 */
void MFRC630_AN1102_RecommendedRegisters(const uint8_t protocol) {
    MFRC630_AN1102_RecommendedRegistersSkip(protocol, 0);
}

void mfrc630_AN1102_recommended_registers_no_transmitter(uint8_t protocol) {
    MFRC630_AN1102_RecommendedRegistersSkip(protocol, 5);
}

// ---------------------------------------------------------------------------
// ISO14443A
// Functions for card wakeup and UID discovery. These functions modify registers
// and do not put them back into the original state. However, if called in the
// right order (MFRC630_ISO14443A_WUPA_REQA(), MFRC630_ISO14443A_Select(), and
// then a function from mifare) the registers should be in the right state. In
// the ISO norm the terms PCD and PICC are used. PCD stands for Proximity
// Coupling Device, by which they mean the reader device. The PICC is the
// Proximity Integrated Circuit Card, so the RFID tag / card. I chose to adhere
// the terminology from the MFRC630 datasheet, which simply refers to 'card' and
// 'reader'.
// ---------------------------------------------------------------------------

/*
 * Sends an Request Command, Type A. This sends the ISO14443 REQA request, cards
 * in IDLE mode should answer to this. 
 * INPUT: none
 * OUTPUT: uint16_t - The Answer to request A byte (ATQA), or zero in case of no
 *          answer.
 */
uint16_t MFRC630_ISO14443A_REQA() {
    return MFRC630_ISO14443A_WUPA_REQA(MFRC630_ISO14443_CMD_REQA);
}

/*
 * Sends an Wake-Up Command, Type A. This sends the ISO14443 WUPA request, cards
 * in IDLE or HALT mode should answer to this. 
 * INPUT: none
 * OUTPUT: uint16_t - The Answer to request A byte (ATQA), or zero in case of no
 *          answer.
 */
uint16_t MFRC630_ISO14443A_WUPA() {
    return MFRC630_ISO14443A_WUPA_REQA(MFRC630_ISO14443_CMD_WUPA);
}

/*
 * Used to send WUPA and REQA. This actually sends WUPA and REQA and returns the
 * response byte.
 * INPUT: const uint8_t instruction - 
 * OUTPUT: uint16_t - The Answer to request A byte (ATQA), or zero in case of no
 *          answer.
 */
uint16_t MFRC630_ISO14443A_WUPA_REQA(const uint8_t instruction) {
    MFRC630Cmd_Idle();
    // mfrc630_AN1102_recommended_registers_no_transmitter(MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER);
    MFRC630FlushFIFO();

    // Set register such that we sent 7 bits, set DataEn such that we can send
    // data.
    MFRC630WriteReg(7 | MFRC630_TXDATANUM_DATAEN, MFRC630_REG_TXDATANUM);

    // disable the CRC registers.
    MFRC630WriteReg(MFRC630_RECOM_14443A_CRC | MFRC630_CRC_OFF, MFRC630_REG_TXCRCPRESET);
    MFRC630WriteReg(MFRC630_RECOM_14443A_CRC | MFRC630_CRC_OFF, MFRC630_REG_RXCRCCON);

    MFRC630WriteReg(0, MFRC630_REG_RXBITCTRL);

    // ready the request.
    uint8_t send_req[];
    send_req[0] = instruction;

    // clear interrupts
    MFRC630ClearIRQ0();
    MFRC630ClearIRQ1();

    // enable the global IRQ for Rx done and Errors.
    MFRC630WriteReg(MFRC630_IRQ0EN_RX_IRQEN | MFRC630_IRQ0EN_ERR_IRQEN, MFRC630_REG_IRQ0EN);
    MFRC630WriteReg(MFRC630_IRQ1EN_TIMER0_IRQEN, MFRC630_REG_IRQ1EN);  // only trigger on timer for irq1

    // configure a timeout timer.
    uint8_t timer_for_timeout = 0;

    // Set timer to 221 kHz clock, start at the end of Tx.
    MFRC630TimerSetControl(timer_for_timeout, MFRC630_TCONTROL_CLK_211KHZ | MFRC630_TCONTROL_START_TX_END);
    // Frame waiting time: FWT = (256 x 16/fc) x 2 FWI
    // FWI defaults to four... so that would mean wait for a maximum of ~ 5ms

    MFRC630TimerSetReload(timer_for_timeout, 1000);  // 1000 ticks of 5 usec is 5 ms.
    MFRC630TimerSetValue(timer_for_timeout, 1000);

    // Go into send, then straight after in receive.
    MFRC630Cmd_Transceive(send_req, 1);
  
    // block until we are done
    uint8_t irq1_value = 0;
    while (!(irq1_value & (1 << timer_for_timeout))) {
        irq1_value = MFRC630_IRQ1();
        if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {  // either ERR_IRQ or RX_IRQ
            break;  // stop polling irq1 and quit the timeout loop.
        }
    }
  
    MFRC630Cmd_Idle();

    // if no Rx IRQ, or if there's an error somehow, return 0
    uint8_t irq0 = MFRC630_IRQ0();
    if ((!(irq0 & MFRC630_IRQ0_RX_IRQ)) || (irq0 & MFRC630_IRQ0_ERR_IRQ)) {
        return 0;
    }

    uint8_t rx_len = MFRC630FIFOLength();
    uint16_t res;
  
    if (rx_len == 2) {  // ATQA should answer with 2 bytes.
        MFRC630ReadFIFO((uint8_t*) &res, rx_len);
        return res;
    }
  
    return 0;
}

/*
 * Performs the SELECT procedure to discover a card's UID. This performs the
 * SELECT procedure as explained in ISO 14443A, this determines the UID of the
 * card, if multiple cards are present, a collision will occur, which is handled
 * according to the norm. This collision handling is explained quite complex in
 * the norm, but conceptually it is not all that complex:
 * - The cascade level can be seen as a prefix to ensure both the PICC and PCD
 *   are working on identifying the same part of the UID.
 * - The entire anti-collision scheme is more of a binary search, the PICC sends
 *   the CASCADE level prefix, then the NVB byte, this field determines how many
 *   bits of the UID will follow, this allows the PICC's to listen to this and
 *   respond if their UID's match these first bits with the UID that is
 *   transmitted. After this all PICC's (that have matched the UID bits already
 *   sent) respond with the remainder of their UIDS. This results in either a
 *   complete UID, or in case two PICC's share a few bits but then differ a bit,
 *   a collision occurs on this bit. This collision is detected by the PCD, at
 *   which point it can chose either to pursue the PICC(s) that has a 0b1 at
 *   that position, or pursue the 0b0 at that position. The ISO norm states: A
 *   typical implementation adds a (1)b. I use the bit value that's in the
 *   pointer at the same position as the collision, or at least for the first
 *   cascade level that works, after that it's off by a byte because of the
 *   cascade tag, see the actual implementation. 
 * INPUT: uint8_t uid - The UID of the card will be stored into this array. This
 *         array is also used to determine the choice between an 0b1 or 0b0 when
 *         a collision occurs. The bit that's in `uid` at the collision position
 *         is selected.
 *        uint8_t sak - The last SAK byte received during the SELECT procedure
 *         is placed here, this often holds information about the type of card.
 * OUTPUT: uint8_t - The length of the UID in bytes (4, 7, 10), or 0 in case of
 *          failure.
 */
uint8_t MFRC630_ISO14443A_Select(uint8_t* uid, uint8_t* sak) {
    MFRC630Cmd_Idle();
    // mfrc630_AN1102_recommended_registers_no_transmitter(MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER);
    MFRC630FlushFIFO();

    // we do not need atqa.
    // Bitshift to get uid_size; 0b00: single, 0b01: double, 0b10: triple, 0b11 RFU
    // uint8_t uid_size = (atqa & (0b11 << 6)) >> 6;
    // uint8_t bit_frame_collision = atqa & 0b11111;

    // enable the global IRQ for Rx done and Errors.
    MFRC630WriteReg(MFRC630_IRQ0EN_RX_IRQEN | MFRC630_IRQ0EN_ERR_IRQEN, MFRC630_REG_IRQ0EN);
    MFRC630WriteReg(MFRC630_IRQ1EN_TIMER0_IRQEN, MFRC630_REG_IRQ1EN);  // only trigger on timer for irq1

    // configure a timeout timer, use timer 0.
    uint8_t timer_for_timeout = 0;

    // Set timer to 221 kHz clock, start at the end of Tx.
    MFRC630TimerSetControl(timer_for_timeout, MFRC630_TCONTROL_CLK_211KHZ | MFRC630_TCONTROL_START_TX_END);
    // Frame waiting time: FWT = (256 x 16/fc) x 2 FWI
    // FWI defaults to four... so that would mean wait for a maximum of ~ 5ms

    MFRC630TimerSetReload(timer_for_timeout, 1000);  // 1000 ticks of 5 usec is 5 ms.
    MFRC630TimerSetValue(timer_for_timeout, 1000);
    uint8_t cascade_level;
    for (cascade_level=1; cascade_level <= 3; cascade_level++) {
        uint8_t cmd = 0;
        uint8_t known_bits = 0;  // known bits of the UID at this level so far.
        uint8_t send_req[7] = {0};  // used as Tx buffer.
        uint8_t* uid_this_level = &(send_req[2]);
        // pointer to the UID so far, by placing this pointer in the send_req
        // array we prevent copying the UID continuously.
        uint8_t message_length;

        switch (cascade_level) {
            case 1:
                cmd = MFRC630_ISO14443_CAS_LEVEL_1;
                break;
            case 2:
                cmd = MFRC630_ISO14443_CAS_LEVEL_2;
                break;
            case 3:
                cmd = MFRC630_ISO14443_CAS_LEVEL_3;
                break;
        }

        // disable CRC in anticipation of the anti collision protocol
        MFRC630WriteReg(MFRC630_RECOM_14443A_CRC | MFRC630_CRC_OFF, MFRC630_REG_TXCRCPRESET);
        MFRC630WriteReg(MFRC630_RECOM_14443A_CRC | MFRC630_CRC_OFF, MFRC630_REG_RXCRCCON);

        // max 32 loops of the collision loop.
        uint8_t collision_n;
        for (collision_n=0; collision_n < 32; collision_n++) {
            // clear interrupts
            MFRC630ClearIRQ0();
            MFRC630ClearIRQ1();

            send_req[0] = cmd;
            send_req[1] = 0x20 + known_bits;
            // send_req[2..] are filled with the UID via the uid_this_level pointer.

            // Only transmit the last 'x' bits of the current byte we are discovering
            // First limit the txdatanum, such that it limits the correct number of bits.
            MFRC630WriteReg((known_bits % 8) | MFRC630_TXDATANUM_DATAEN, MFRC630_REG_TXDATANUM);

            // set it to insert zeros in case of a collision, (1<<7)
            // We want to shift the bits with RxAlign
            uint8_t rxalign = known_bits % 8;
            MFRC630WriteReg((1<<7) | (rxalign<<4), MFRC630_REG_RXBITCTRL);

            // then sent the send_req to the hardware,
            // (known_bits / 8) + 1): The ceiled number of bytes by known bits.
            // +2 for cmd and NVB.
            if ((known_bits % 8) == 0) {
                message_length = ((known_bits / 8)) + 2;
            }
            else {
                message_length = ((known_bits / 8) + 1) + 2;
            }

            MFRC630Cmd_Transceive(send_req, message_length);

            // block until we are done
            uint8_t irq1_value = 0;
            while (!(irq1_value & (1 << timer_for_timeout))) {
                irq1_value = MFRC630_IRQ1();
                // either ERR_IRQ or RX_IRQ or Timer
                if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
                    break;  // stop polling irq1 and quit the timeout loop.
                }
            }
            MFRC630Cmd_Idle();

            // next up, we have to check what happened.
            uint8_t irq0 = MFRC630_IRQ0();
            uint8_t error = MFRC630ReadReg(MFRC630_REG_ERROR);
            uint8_t coll = MFRC630ReadReg(MFRC630_REG_RXCOLL);
            
            uint8_t collision_pos = 0;
            if (irq0 & MFRC630_IRQ0_ERR_IRQ) {  // some error occured.
                // Check what kind of error.
                // error = mfrc630_read_reg(MFRC630_REG_ERROR);
                if (error & MFRC630_ERROR_COLLDET) {
                    // A collision was detected...
                    if (coll & (1<<7)) {
                        collision_pos = coll & (~(1<<7));

                        // This be a true collision... we have to select either the address
                        // with 1 at this position or with zero
                        // ISO spec says typically a 1 is added, that would mean:
                        // uint8_t selection = 1;

                        // However, it makes sense to allow some kind of user input for this, so we use the
                        // current value of uid at this position, first index right byte, then shift such
                        // that it is in the rightmost position, ten select the last bit only.
                        // We cannot compensate for the addition of the cascade tag, so this really
                        // only works for the first cascade level, since we only know whether we had
                        // a cascade level at the end when the SAK was received.
                        uint8_t choice_pos = known_bits + collision_pos;
                        uint8_t selection = (uid[((choice_pos + (cascade_level-1)*3)/8)] >> ((choice_pos) % 8))&1;

                        // We just OR this into the UID at the right position, later we
                        // OR the UID up to this point into uid_this_level.
                        uid_this_level[((choice_pos)/8)] |= selection << ((choice_pos) % 8);
                        known_bits++;  // add the bit we just decided.
                    }
                    else {
                        // Datasheet of mfrc630:
                        // bit 7 (CollPosValid) not set:
                        // Otherwise no collision is detected or
                        // the position of the collision is out of the range of bits CollPos.

                        collision_pos = 0x20 - known_bits;
                    }
                }
                else {
                    // Can this ever occur?
                    collision_pos = 0x20 - known_bits;
                }
            }
            else if (irq0 & MFRC630_IRQ0_RX_IRQ) {
                // we got data, and no collisions, that means all is well.
                collision_pos = 0x20 - known_bits;
            }
            else {
                // We have no error, nor received an RX. No response, no card?
                return 0;
            }

            // read the UID Cln so far from the buffer.
            uint8_t rx_len = MFRC630FIFOLength();
            uint8_t buf[5];  // Size is maximum of 5 bytes, UID[0-3] and BCC.

            MFRC630ReadFIFO(buf, rx_len < 5 ? rx_len : 5);

            // move the buffer into the uid at this level, but OR the result such that
            // we do not lose the bit we just set if we have a collision.
            uint8_t rbx;
            for (rbx = 0; (rbx < rx_len); rbx++) {
                uid_this_level[(known_bits / 8) + rbx] |= buf[rbx];
            }
            known_bits += collision_pos;

            if ((known_bits >= 32)) {
                break;  // done with collision loop
            }
        }  // end collision loop

        // check if the BCC matches
        uint8_t bcc_val = uid_this_level[4];  // always at position 4, either with CT UID[0-2] or UID[0-3] in front.
        uint8_t bcc_calc = uid_this_level[0]^uid_this_level[1]^uid_this_level[2]^uid_this_level[3];
        if (bcc_val != bcc_calc) {
            return 0;
        }

        // clear interrupts
        MFRC630ClearIRQ0();
        MFRC630ClearIRQ1();

        send_req[0] = cmd;
        send_req[1] = 0x70;
        // send_req[2,3,4,5] // contain the CT, UID[0-2] or UID[0-3]
        send_req[6] = bcc_calc;
        message_length = 7;

        // Ok, almost done now, we reenable the CRC's
        MFRC630WriteReg(MFRC630_RECOM_14443A_CRC | MFRC630_CRC_ON, MFRC630_REG_TXCRCPRESET);
        MFRC630WriteReg(MFRC630_RECOM_14443A_CRC | MFRC630_CRC_ON, MFRC630_REG_RXCRCCON);

        // reset the Tx and Rx registers (disable alignment, transmit full bytes)
        MFRC630WriteReg((known_bits % 8) | MFRC630_TXDATANUM_DATAEN, MFRC630_REG_TXDATANUM);
        uint8_t rxalign = 0;
        MFRC630WriteReg((1 << 7) | (rxalign << 4), MFRC630_REG_RXBITCTRL);

        // actually send it!
        MFRC630Cmd_Transceive(send_req, message_length);

        // Block until we are done...
        uint8_t irq1_value = 0;
        while (!(irq1_value & (1 << timer_for_timeout))) {
            irq1_value = MFRC630_IRQ1();
            if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {  // either ERR_IRQ or RX_IRQ
                break;  // stop polling irq1 and quit the timeout loop.
            }
        }
        MFRC630Cmd_Idle();

        // Check the source of exiting the loop.
        uint8_t irq0_value = MFRC630_IRQ0();
        if (irq0_value & MFRC630_IRQ0_ERR_IRQ) {
            // Check what kind of error.
            uint8_t error = MFRC630ReadReg(MFRC630_REG_ERROR);
            if (error & MFRC630_ERROR_COLLDET) {
                // a collision was detected with NVB=0x70, should never happen.
                return 0;
            }
        }

        // Read the sak answer from the fifo.
        uint8_t sak_len = MFRC630FIFOLength();
        if (sak_len != 1) {
            return 0;
        }
        uint8_t sak_value;
        MFRC630ReadFIFO(&sak_value, sak_len);

        if (sak_value & (1 << 2)) {
            // UID not yet complete, continue with next cascade.
            // This also means the 0'th byte of the UID in this level was CT, so we
            // have to shift all bytes when moving to uid from uid_this_level.
            uint8_t UIDn;
            for (UIDn=0; UIDn < 3; UIDn++) {
                // uid_this_level[UIDn] = uid_this_level[UIDn + 1];
                uid[(cascade_level-1)*3 + UIDn] = uid_this_level[UIDn + 1];
            }
        }
        else {
            // Done according so SAK!
            // Add the bytes at this level to the UID.
            uint8_t UIDn;
            for (UIDn=0; UIDn < 4; UIDn++) {
                uid[(cascade_level-1)*3 + UIDn] = uid_this_level[UIDn];
            }

            // Finally, return the length of the UID that's now at the uid pointer.
            return cascade_level*3 + 1;
        }
    }  // cascade loop
  
    return 0;  // getting a UID failed.
}

// ---------------------------------------------------------------------------
// MIFARE
// ---------------------------------------------------------------------------
/*
uint8_t mfrc630_MF_auth(const uint8_t* uid, uint8_t key_type, uint8_t block) {
  // Enable the right interrupts.

  // configure a timeout timer.
  uint8_t timer_for_timeout = 0;  // should match the enabled interupt.

  // According to datashet Interrupt on idle and timer with MFAUTHENT, but lets
  // include ERROR as well.
  mfrc630_write_reg(MFRC630_REG_IRQ0EN, MFRC630_IRQ0EN_IDLE_IRQEN | MFRC630_IRQ0EN_ERR_IRQEN);
  mfrc630_write_reg(MFRC630_REG_IRQ1EN, MFRC630_IRQ1EN_TIMER0_IRQEN);  // only trigger on timer for irq1

  // Set timer to 221 kHz clock, start at the end of Tx.
  mfrc630_timer_set_control(timer_for_timeout, MFRC630_TCONTROL_CLK_211KHZ | MFRC630_TCONTROL_START_TX_END);
  // Frame waiting time: FWT = (256 x 16/fc) x 2 FWI
  // FWI defaults to four... so that would mean wait for a maximum of ~ 5ms

  mfrc630_timer_set_reload(timer_for_timeout, 2000);  // 2000 ticks of 5 usec is 10 ms.
  mfrc630_timer_set_value(timer_for_timeout, 2000);

  uint8_t irq1_value = 0;

  mfrc630_clear_irq0();  // clear irq0
  mfrc630_clear_irq1();  // clear irq1

  // start the authentication procedure.
  mfrc630_cmd_auth(key_type, block, uid);

  // block until we are done
  while (!(irq1_value & (1 << timer_for_timeout))) {
    irq1_value = mfrc630_irq1();
    if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
      break;  // stop polling irq1 and quit the timeout loop.
    }
  }

  if (irq1_value & (1 << timer_for_timeout)) {
    // this indicates a timeout
    return 0;  // we have no authentication
  }

  // status is always valid, it is set to 0 in case of authentication failure.
  uint8_t status = mfrc630_read_reg(MFRC630_REG_STATUS);
  return (status & MFRC630_STATUS_CRYPTO1_ON);
}

uint8_t mfrc630_MF_read_block(uint8_t block_address, uint8_t* dest) {
  mfrc630_flush_fifo();

  mfrc630_write_reg(MFRC630_REG_TXCRCPRESET, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_ON);
  mfrc630_write_reg(MFRC630_REG_RXCRCCON, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_ON);

  uint8_t send_req[2] = {MFRC630_MF_CMD_READ, block_address};

  // configure a timeout timer.
  uint8_t timer_for_timeout = 0;  // should match the enabled interupt.

  // enable the global IRQ for idle, errors and timer.
  mfrc630_write_reg(MFRC630_REG_IRQ0EN, MFRC630_IRQ0EN_IDLE_IRQEN | MFRC630_IRQ0EN_ERR_IRQEN);
  mfrc630_write_reg(MFRC630_REG_IRQ1EN, MFRC630_IRQ1EN_TIMER0_IRQEN);


  // Set timer to 221 kHz clock, start at the end of Tx.
  mfrc630_timer_set_control(timer_for_timeout, MFRC630_TCONTROL_CLK_211KHZ | MFRC630_TCONTROL_START_TX_END);
  // Frame waiting time: FWT = (256 x 16/fc) x 2 FWI
  // FWI defaults to four... so that would mean wait for a maximum of ~ 5ms
  mfrc630_timer_set_reload(timer_for_timeout, 2000);  // 2000 ticks of 5 usec is 10 ms.
  mfrc630_timer_set_value(timer_for_timeout, 2000);

  uint8_t irq1_value = 0;
  uint8_t irq0_value = 0;

  mfrc630_clear_irq0();  // clear irq0
  mfrc630_clear_irq1();  // clear irq1

  // Go into send, then straight after in receive.
  mfrc630_cmd_transceive(send_req, 2);

  // block until we are done
  while (!(irq1_value & (1 << timer_for_timeout))) {
    irq1_value = mfrc630_irq1();
    if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
      break;  // stop polling irq1 and quit the timeout loop.
    }
  }
  mfrc630_cmd_idle();

  if (irq1_value & (1 << timer_for_timeout)) {
    // this indicates a timeout
    return 0;
  }

  irq0_value = mfrc630_irq0();
  if (irq0_value & MFRC630_IRQ0_ERR_IRQ) {
    // some error
    return 0;
  }

  // all seems to be well...
  uint8_t buffer_length = mfrc630_fifo_length();
  uint8_t rx_len = (buffer_length <= 16) ? buffer_length : 16;
  mfrc630_read_fifo(dest, rx_len);
  return rx_len;
}

// The read and write block functions share a lot of code, the parts they have in common could perhaps be extracted
// to make it more readable.

uint8_t mfrc630_MF_write_block(uint8_t block_address, const uint8_t* source) {
  mfrc630_flush_fifo();

  // set appropriate CRC registers, only for Tx
  mfrc630_write_reg(MFRC630_REG_TXCRCPRESET, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_ON);
  mfrc630_write_reg(MFRC630_REG_RXCRCCON, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_OFF);
  // configure a timeout timer.
  uint8_t timer_for_timeout = 0;  // should match the enabled interupt.

  // enable the global IRQ for idle, errors and timer.
  mfrc630_write_reg(MFRC630_REG_IRQ0EN, MFRC630_IRQ0EN_IDLE_IRQEN | MFRC630_IRQ0EN_ERR_IRQEN);
  mfrc630_write_reg(MFRC630_REG_IRQ1EN, MFRC630_IRQ1EN_TIMER0_IRQEN);

  // Set timer to 221 kHz clock, start at the end of Tx.
  mfrc630_timer_set_control(timer_for_timeout, MFRC630_TCONTROL_CLK_211KHZ | MFRC630_TCONTROL_START_TX_END);
  // Frame waiting time: FWT = (256 x 16/fc) x 2 FWI
  // FWI defaults to four... so that would mean wait for a maximum of ~ 5ms
  mfrc630_timer_set_reload(timer_for_timeout, 2000);  // 2000 ticks of 5 usec is 10 ms.
  mfrc630_timer_set_value(timer_for_timeout, 2000);

  uint8_t irq1_value = 0;
  uint8_t irq0_value = 0;

  uint8_t res;

  uint8_t send_req[2] = {MFRC630_MF_CMD_WRITE, block_address};

  mfrc630_clear_irq0();  // clear irq0
  mfrc630_clear_irq1();  // clear irq1

  // Go into send, then straight after in receive.
  mfrc630_cmd_transceive(send_req, 2);

  // block until we are done
  while (!(irq1_value & (1 << timer_for_timeout))) {
    irq1_value = mfrc630_irq1();
    if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
      break;  // stop polling irq1 and quit the timeout loop.
    }
  }
  mfrc630_cmd_idle();

  // check if the first stage was successful:
  if (irq1_value & (1 << timer_for_timeout)) {
    // this indicates a timeout
    return 0;
  }
  irq0_value = mfrc630_irq0();
  if (irq0_value & MFRC630_IRQ0_ERR_IRQ) {
    // some error
    return 0;
  }
  uint8_t buffer_length = mfrc630_fifo_length();
  if (buffer_length != 1) {
    return 0;
  }
  mfrc630_read_fifo(&res, 1);
  if (res != MFRC630_MF_ACK) {
    return 0;
  }

  mfrc630_clear_irq0();  // clear irq0
  mfrc630_clear_irq1();  // clear irq1

  // go for the second stage.
  mfrc630_cmd_transceive(source, 16);

  // block until we are done
  while (!(irq1_value & (1 << timer_for_timeout))) {
    irq1_value = mfrc630_irq1();
    if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
      break;  // stop polling irq1 and quit the timeout loop.
    }
  }

  mfrc630_cmd_idle();

  if (irq1_value & (1 << timer_for_timeout)) {
    // this indicates a timeout
    return 0;
  }
  irq0_value = mfrc630_irq0();
  if (irq0_value & MFRC630_IRQ0_ERR_IRQ) {
    // some error
    return 0;
  }

  buffer_length = mfrc630_fifo_length();
  if (buffer_length != 1) {
    return 0;
  }
  mfrc630_read_fifo(&res, 1);
  if (res == MFRC630_MF_ACK) {
    return 16;  // second stage was responded with ack! Write successful.
  }

  return 0;
}

void mfrc630_MF_deauth() {
  mfrc630_write_reg(MFRC630_REG_STATUS, 0);
}

void mfrc630_MF_example_dump() {
  uint16_t atqa = mfrc630_iso14443a_REQA();
  if (atqa != 0) {  // Are there any cards that answered?
    uint8_t sak;
    uint8_t uid[10] = {0};  // uids are maximum of 10 bytes long.

    // Select the card and discover its uid.
    uint8_t uid_len = mfrc630_iso14443a_select(uid, &sak);

    if (uid_len != 0) {  // did we get an UID?
      MFRC630_PRINTF("UID of %hhd bytes (SAK:0x%hhX): ", uid_len, sak);
      mfrc630_print_block(uid, uid_len);
      MFRC630_PRINTF("\n");

      // Use the manufacturer default key...
      uint8_t FFkey[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

      mfrc630_cmd_load_key(FFkey);  // load into the key buffer

      // Try to athenticate block 0.
      if (mfrc630_MF_auth(uid, MFRC630_MF_AUTH_KEY_A, 0)) {
        MFRC630_PRINTF("Yay! We are authenticated!\n");

        // Attempt to read the first 4 blocks.
        uint8_t readbuf[16] = {0};
        uint8_t len;
        uint8_t b;
        for (b=0; b < 4 ; b++) {
          len = mfrc630_MF_read_block(b, readbuf);
          MFRC630_PRINTF("Read block 0x%hhX: ", b);
          mfrc630_print_block(readbuf, len);
          MFRC630_PRINTF("\n");
        }
        mfrc630_MF_deauth();  // be sure to call this after an authentication!
      } else {
        MFRC630_PRINTF("Could not authenticate :(\n");
      }
    } else {
      MFRC630_PRINTF("Could not determine UID, perhaps some cards don't play");
      MFRC630_PRINTF(" well with the other cards? Or too many collisions?\n");
    }
  } else {
    MFRC630_PRINTF("No answer to REQA, no cards?\n");
  }
}
*/
