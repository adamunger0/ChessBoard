/*
 * NAME: PinMagic.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: Initialize all of the ports on the PIC18LF46K22.
 *  PINS: RA0: /COIL_EN0 digital output high (default as connected to ground)
 *		    1: /COIL_EN1 digital output high (default as connected to ground)
 *		    2: 
 *		    3: 
 *		    4: LED0 digital output low
 *		    5: LED1 digital output low
 *		    6: 
 *		    7: 
 *		  RB0: 
 *		    1:
 *		    2:
 *		    3:
 *		    4:
 *		    5:
 *		    6: PGC:
 *		    7: PGD:
 *		  RC0: 
 *		    1:
 *		    2: 
 *		    3: SCL1 MFRC630 digital input low
 *		    4: SDA1 MFRC630 digital input low
 *		    5: 
 *		    6: PDOWN MFRC630 digital output low
 *		    7:
 *		  RD0: 
 *		    1: 
 *		    2: 
 *		    3: 
 *		    4:
 *		    5:
 *		    6: 
 *		    7: 
 *		  RE0:
 *		    1:
 *		    2:
 *		    3: MCLR:
 *		    4: N/A
 *		    5: N/A
 *		    6: N/A
 *		    7: N/A
 */

#include <xc.h>

/*
 * Initialize the ports on the PIC micro. See description of this file for
 * explanations.
 * INPUT: none
 * OUTPUT: none
 */
static void initPorts() {
    // set the ports as analog (1) or digital (0)
    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;
    ANSELD = 0;
    ANSELE = 0;

    // set the initial output value to be written to the ports when they're TRIS'd
    PORTA = 0b00000011;
    PORTB = 0;
    PORTC = 0;
    PORTD = 0;
    PORTE = 0;

    // TRIS the ports as input (1) or output (0)
    TRISA = 0;
    TRISB = 0;
    TRISC = 0b00011000; // RC3 (SCL1), RC4 (SDA1), see pg. 138 of PIC manual
    TRISD = 0;
    TRISE = 0;
}


/*
 * This will setup the micro to function as expected for the chessboard project.
 * This includes a 64MHz clock speed, enter sleep mode on SLEEP(), and
 * initialize the ports.
 * INPUT: none
 * OUTPUT: none
 */
void setupMicro() {
    initPorts();

    // setup oscillator for 64MHz operation
    OSCCONbits.IDLEN = 0; // enter sleep mode on sleep instruction
    OSCCONbits.HFIOFS = 1; // HFINTOSC is stable
    OSCCONbits.IRCF = 7; // HF internal clock - 16MHz * 4 (PLL) = 64MHz
    OSCTUNEbits.PLLEN = 1; // 4x multiplier enabled
}

