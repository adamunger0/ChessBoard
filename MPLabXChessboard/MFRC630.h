/* 
 * File:   MFRC630.h
 * Author: Adam Unger
 * Description: This file defines the constants and functions for interacting
 *  with the MFRC630. Much of it's contents come from here:
 *  https://github.com/iwanders/MFRC630
 */

#ifndef MFRC630_H_
#define	MFRC630_H_

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h> // stdint**_t
    
#include "MFRC630_def.h" // register and constant definitions

    
// ---------------------------------------------------------------------------
// Register interaction functions.
//   Manipulate the chip's registers. These functions use the SPI communication
//   functions to interact with the hardware. All interaction with the chip goes
//   through these functions. In case another interface than SPI is to be used,
//   these functions can be adapted.
// ---------------------------------------------------------------------------

/*
 * Reads a register.
 * INPUT: const uint8_t reg - Specifies which register to read.
 * OUTPUT: uint8_t - the value of the register to be read.
 */
uint8_t MFRC630ReadReg(const uint8_t reg);

/*
 * Write a register. Sets a single register to the provided value.
 * INPUT: const uint8_t data - Specifies the value to write to that register.
 *        const uint8_t reg - Specifies which register to write.
 * OUTPUT: none
 */
void MFRC630WriteReg(const uint8_t data, const uint8_t reg);


/*
 * Write multiple registers. Sets consecutive registers to the provided values. 
 * INPUT: const uint8_t* data - An array of the values to write to the register
 *          starting from `reg`. The first value (`values[0]`) gets written to
 *          `reg`, the second (`values[1]`) to `reg+1`, and so on.
 *        const uint8_t reg - Specifies at which register writing starts.        
 *        const uint16_t len - The number of register to write.
 * OUTPUT: none
 */
void MFRC630WriteRegs(const uint8_t* data, const uint8_t reg, const uint16_t len);

/*
 * Write data to FIFO. The FIFO is located at register `#MFRC630_REG_FIFODATA`. 
 * Writes to this register do not automatically increment the write pointer in
 * the chip and multiple bytes may be written to this register to place them
 * into the FIFO buffer. This function does not clear the FIFO beforehand, it
 * only provides the raw transfer functionality. 
 * INPUT: const uint8_t* data - The data to be written into the FIFO.
 *        const uint16_t len - The number of bytes to be written into the FIFO.
 * OUTPUT: none
 */
void MFRC630WriteFIFO(const uint8_t* data, const uint16_t len);

/*
 * Read data from FIFO. This function reads data from the FIFO into an array on
 * the microcontroller. NOTE: This reads regardless of #MFRC630_REG_FIFOLENGTH,
 * if there aren't enough bytes present in the FIFO, they are read from the chip
 * anyway, these bytes should not be used. (The returned bytes from an empty 
 * FIFO are often identical to the last valid byte that was read from it.)
 * INPUT: uint8_t* data - The data read from the FIFO is placed into this array.
 *        const uint16_t len - The number of bytes to be read from the FIFO.
 * OUTPUT: none
 */
void MFRC630ReadFIFO(uint8_t* data, const uint16_t len);

// ---------------------------------------------------------------------------
// Command functions
//   These activate the various commands of the chip with the right arguments.
//   The chip has various commands it can execute, these commands often have
//   arguments which should be transferred to the FIFO before the command is
//   initiated. These functions provide this functionality of transferring the
//   arguments to the FIFO and then initiating the commands.
// ---------------------------------------------------------------------------

/*
 * Read data from EEPROM into the FIFO buffer. This instruction transfers data
 * from the EEPROM (section 2) at the given address location into the FIFO
 * buffer.
 * INPUT: const uint16_t address - The start address in the EEPROM to start
 *        reading from.
 *        const uint16_t length - The number of bytes to read from the EEPROM
 *        into the FIFO buffer.
 * OUTPUT: none
 */
void MFRC630Cmd_ReadE2(const uint16_t address, const uint16_t length);

/*
 * Write a byte to the EEPROM. This instruction transfers a single byte into the
 * EEPROM at the specified address.
 * INPUT: const uint16_t address - The address in the EEPROM to write to.
 *        const uint8_t data - The data which is destined for the EEPROM.
 * OUTPUT: none
 */
void MFRC630Cmd_WriteE2(const uint16_t address, const uint8_t data);

/*!
  @brief Read data from EEPROM into the registers.
  
  This instruction transfers data from the EEPROM (section 2) at the provided address into the registers..
  \param [in] address The start address in the EEPROM to start reading from.
  \param [in] regaddr The start address of the register to start writing into.
  \param [in] length The number of bytes to read and registers to write consecutively from `regaddr`.
 */
void MFRC630Cmd_LoadReg(const uint16_t address, const uint8_t regaddr, const uint16_t length);

/*
 * Load protocol settings. Loads register settings for the protocol indicated.
 * Can configure different protocols for rx and tx. The most common protocol is
 * MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER which is the default protocol
 * for the SELECT procedure. The most common protocols are listed in the
 * datasheet, but the MFRC630 Quickstart Guide AN11022 gives a complete 
 * description. 
 * INPUT: const uint8_t rx - The protocol number to load for the receiving
 *         frontend.
 *        const uint8_t tx - The protocol number to load for the transmitting
 *          frontend.
 * OUTPUT: none
 */
void MFRC630Cmd_LoadProtocol(const uint8_t rx, const uint8_t tx);

/*
 * Turn on the receive circuit by issuing the MFRC630_CMD_RECEIVE command.
 * INPUT: none
 * OUTPUT: data in FIFO buffer
 */
void MFRC630Cmd_Receive();

/*
 * Transmit the bytes provided and go into receive mode. This function loads the
 * data from the `data` array into the FIFO, and then issues the
 * `MFRC630_CMD_TRANSCEIVE` command, which sends the data in the FIFO and
 * switches to receiving mode afterwards.
 * INPUT: const uint8_t* data - The data to be transmitted.
 *      const uint16_t len - The number of bytes to be read from `data` and be
 *       transmitted.
 * OUTPUT: data in FIFO buffer
 */
void MFRC630Cmd_Transceive(const uint8_t* data, const uint16_t len);

/*!
  @brief Set the device into idle mode.
  Stops the currently active command and return to idle mode.
 */
void MFRC630Cmd_Idle();

/*!
  @brief Loads a key from the EEPROM into the key buffer.
  This function can load a key from the MIFARE key area in the EEPROM into the key buffer. This section of the EEPROM
  can only be written. The key buffer is a part of memory in the MFRC630 used for the MIFARE authentication procedure.
  \param [in] key_nr Loads the key stored for this index.
 */
void MFRC630Cmd_LoadKeyE2(const uint8_t key_nr);

/*!
  @brief Loads the provided key into the key buffer.
  This function reads 6 bytes from the `key` array into the FIFO and then loads the key into the key buffer.
  \param [in] key Array which holds the MIFARE key, it is always 6 bytes long.
 */
void MFRC630Cmd_LoadKey(const uint8_t* key);

/*!
  @brief Perform MIFARE authentication procedure with a card.
  This function attemps to authenticate with the specified card using the key which is currently in the key buffer.
  This function is usually preceded by either the `mfrc630_cmd_load_key_E2()` or `mfrc630_cmd_load_key()` functions.
  \param [in] key_type The MIFARE key A or B (`MFRC630_MF_AUTH_KEY_A` = 0x60 or `MFRC630_MF_AUTH_KEY_B` = 0x61) to use.
  \param [in] block_address The block on which to authenticate.
  \param [in] card_uid The authentication procedure required the first four bytes of the card's UID to authenticate.
 */
void MFRC630Cmd_Auth(const uint8_t key_type, const uint8_t block_address, const uint8_t* card_uid);

//! @}

// ---------------------------------------------------------------------------
// Utility functions
// ---------------------------------------------------------------------------
/*! \defgroup utility Utility
    \brief Various utility functions for often performed actions.
  @{
*/


/*!
  @brief Flush the FIFO buffer.
  This function clears all contents that are currently in the FIFO.
 */
void MFRC630FlushFIFO();

/*!
  @brief Get the FIFO length.
  Returns the current number of bytes in the FIFO.
  \warning This function only returns the first 8 bits of the FIFO length, if the 512 byte FIFO is used, only the least
           significant eight bits will be returned.
  
  \return The number of bytes currently in the FIFO.
 */
uint16_t MFRC630FIFOLength();


/*!
  @brief Clear the interrupt0 flags.
  Resets the interrupt 0 register (`MFRC630_REG_IRQ0`).
 */
void MFRC630ClearIRQ0();
/*!
  @brief Clear the interrupt1 flags.
  Resets the interrupt 1 register (`MFRC630_REG_IRQ1`).
 */
void MFRC630ClearIRQ1();


/*!
  @brief Get the value of the interrupt 0 register.
  \return The value of the `MFRC630_REG_IRQ0` register.
 */
uint8_t MFRC630_IRQ0();

/*!
  @brief Get the value of the interrupt 1 register.
  \return The value of the `MFRC630_REG_IRQ1` register.
 */
uint8_t MFRC630_IRQ1();

/*!
  @brief Copy a page from EEPROM into an array on the MCU.
  
  This instruction transfers a page from the EEPROM into the FIFO and then transfers this data from the FIFO
  into an array. It always transfers 64 bytes, as such `dest` must be (at least) 64 bytes long.
  This basically calls mfrc630_cmd_read_E2() and then transfers the FIFO with mfrc630_read_fifo(). This is useful for
  dumping the entire EEPROM.
  \param [out] dest The array to write the data into.
  \param [in] page The page to read from the EEPROM. (This gets multiplied by 64 to obtain the start address).
  \return The number of bytes transmitted from the FIFO into `dest`.
 */
uint8_t MFRC630TransferE2Page(uint8_t* dest, const uint8_t page);

//!  @}

// ---------------------------------------------------------------------------
// Timer functions
// ---------------------------------------------------------------------------
/*! \defgroup timer Timer
    \brief Functions for manipulating the timers.
    The MFRC630 has 5 timers, the first four can be treated more or less similarly, the last timer `Timer4` has a
    different control register.
    Timer 0-3 can be treated in a similar way, and as such the functions take an argument that specifies which timer
    to manipulate.
    Timer4 is special, read the datasheet on how to use that timer as it has other clock sources and properties.
  @{
*/

/*!
  @brief Activates a timer.
  This sets the the `MFRC630_REG_TCONTROL` register to enable or disable this timer.
  \note Seems to trigger timer reset?
  \param [in] timer Specifies which timer to use (0, 1, 2 or 3).
  \param [in] active Should be `0` to deactivate the timer, `1` to activate it.
 */
void MFRC630ActivateTimer(const uint8_t timer, const uint8_t active);

// Set the timer control field of the timer.
// value: the value to set the timer's control field to.
/*!
  @brief Sets the timer control register.
  This sets the `T[0-3]Control` register to the provided value. The value speficies the propertief of StopRx, Start
  AutoRestart and Clk for this timer.
  \param [in] timer Specifies which timer to use (0, 1, 2 or 3).
  \param [in] value This can be a combination of the defines associated with the Timer controls.
  \see MFRC630_TCONTROL_STOPRX
  \see MFRC630_TCONTROL_START_NOT, MFRC630_TCONTROL_START_TX_END, MFRC630_TCONTROL_START_LFO_WO,
       MFRC630_TCONTROL_START_LFO_WITH
  \see MFRC630_TCONTROL_CLK_13MHZ, MFRC630_TCONTROL_CLK_211KHZ, MFRC630_TCONTROL_CLK_UF_TA1, MFRC630_TCONTROL_CLK_UF_TA2
 */
void MFRC630TimerSetControl(const uint8_t timer, const uint8_t value);

/*!
  @brief Sets the reload value of the timer.
  This counter starts counting down from this reload value, an underflow occurs when the timer reaches zero.
  \param [in] timer Specifies which timer to use (0, 1, 2 or 3).
  \param [in] value The value from which to start the counter. 
 */
void MFRC630TimerSetReload(const uint8_t timer, const uint16_t value);

/*!
  @brief Sets the current value of this timer..
  Sets the current value of this counter, it counts down from this given value.
  \param [in] timer Specifies which timer to use (0, 1, 2 or 3).
  \param [in] value The value to set the counter to. 
 */
void MFRC630TimerSetValue(const uint8_t timer, const uint16_t value);


/*!
  @brief Retrieve the current value of a timer.
  Reads the current value of the given timer and returns the result.
  \param [in] timer Specifies which timer to use (0, 1, 2 or 3).
  \return The current value of this timer.
 */
uint16_t MFRC630TimerGetValue(const uint8_t timer);
//!  @}

// ---------------------------------------------------------------------------
// From documentation
// ---------------------------------------------------------------------------
/*! \defgroup documentation From application notes
    \brief Several functions written based on application notes.
    Two application notes are of particular interest:
      - Application Note 11145:
          CLRC663, MFRC631, MFRC 630, SLRC610 Low Power Card Detection
          http://www.nxp.com/documents/application_note/AN11145.pdf
      - Application Note 11022:
          CLRC663 Quickstart Guide
          http://www.nxp.com/documents/application_note/AN11022.pdf
    The first details how to perform an IQ measurement to determine the thresholds for the Low Power Card Detection
    (LPCD). The second describes default register values for various protocols and the protocol numbers that are
    associated to them.
     
  @{
*/

// From Application Note 11145:
//      CLRC663, MFRC631, MFRC 630, SLRC610 Low Power Card Detection
//      http://www.nxp.com/documents/application_note/AN11145.pdf

//      IQ measurement, section 3.2.1

/*
 * Start IQ Measurement. From Application Note 11145, section 3.2.1, it
 * configures the registers to perform an IQ measurement.
 * INPUT: none
 * OUTPUT: none
 */
void MFRC630_AN11145_StartIQMeasurement();

//      wait about 50ms between them.
/*! \brief Stop IQ Measurement.
    Stop the previously started IQ measurement. The application note uses a delay of 50 ms between the start and stop.
    The actual values can be retrieved with:
    \code
      uint8_t I_value = mfrc630_read_reg(MFRC630_REG_LPCD_I_RESULT) & 0x3F
      uint8_t Q_value = mfrc630_read_reg(MFRC630_REG_LPCD_Q_RESULT) & 0x3F
    \endcode
*/
void mfrc630_AN11145_stop_IQ_measurement();


// From Application Note 11022:
//      CLRC663 Quickstart Guide
//      http://www.nxp.com/documents/application_note/AN11022.pdf

/*
 * Set the registers to the recommended values. This function uses the
 * recommended registers from the datasheets, it should yield identical results
 * to the  MFRC630Cmd_LoadProtocol() function.
 * INPUT: const uint8_t protocol - The protocol index to set the registers to.
 *         Only MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER,
 *         MFRC630_PROTO_ISO14443A_212_MILLER_BPSK,
 *         MFRC630_PROTO_ISO14443A_424_MILLER_BPSK and 
 *         MFRC630_PROTO_ISO14443A_848_MILLER_BPSK were copied. The recommended
 *         values for the other protocols can be found in the application note.
 * OUTPUT: none
 */
void MFRC630_AN1102_RecommendedRegisters(const uint8_t protocol);

/*! \brief Set the registers to the recommended values starting from `MFRC630_REG_TXCRCPRESET`.
    Since the transmitter registers can require hardware-specific customization to work correctly, this function sets the
    recommended values of the registers after the transmitter settings.
    \param [in] protocol The protocol index to set the registers to. Only
           `MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER`, `MFRC630_PROTO_ISO14443A_212_MILLER_BPSK`,
           `MFRC630_PROTO_ISO14443A_424_MILLER_BPSK` and `MFRC630_PROTO_ISO14443A_848_MILLER_BPSK` were copied. The
            recommended values for the other protocols can be found in the application note.
*/
void mfrc630_AN1102_recommended_registers_no_transmitter(uint8_t protocol);

/*
 * Set the registers to the recommended values, skipping the first skip
 * registers. Sets the recommended registers but allows for an arbitrary number
 * of registers to be skipped at the start. 
 * INPUT: const uint8_t protocol - The protocol index to set the registers to.
 *         Only MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER, 
 *         MFRC630_PROTO_ISO14443A_212_MILLER_BPSK,
 *         MFRC630_PROTO_ISO14443A_424_MILLER_BPSK and
 *         MFRC630_PROTO_ISO14443A_848_MILLER_BPSK were copied. The recommended
 *         values for the other protocols can be found in the application note.
 *        const uint8_t skip - The number of registers to skip from the start.
 */
void MFRC630_AN1102_RecommendedRegistersSkip(const uint8_t protocol, const uint8_t skip);

// ---------------------------------------------------------------------------
// ISO14443A
// Functions for card wakeup and UID discovery. These functions modify registers
// and do not put them back into the original state. However, if called in the
// right order (MFRC630_ISO14443A_WUPA_REQA(), MFRC630_ISO14443A_Select(), and
// then a function from mifare) the registers should be in the right state. In
// the ISO norm the terms PCD and PICC are used. PCD stands for Proximity
// Coupling Device, by which they mean the reader device. The PICC is the
// Proximity Integrated Circuit Card, so the RFID tag/card. I chose to adhere
// the terminology from the MFRC630 datasheet, which simply refers to 'card' and
// 'reader'.
// ---------------------------------------------------------------------------

/*
 * Sends an Request Command, Type A. This sends the ISO14443 REQA request, cards
 * in IDLE mode should answer to this. 
 * INPUT: none
 * OUTPUT: uint16_t - The Answer to request A byte (ATQA), or zero in case of no
 *          answer.
 */
uint16_t MFRC630_ISO14443A_REQA();

/*
 * Sends an Wake-Up Command, Type A. This sends the ISO14443 WUPA request, cards
 * in IDLE or HALT mode should answer to this. 
 * INPUT: none
 * OUTPUT: uint16_t - The Answer to request A byte (ATQA), or zero in case of no
 *          answer.
 */
uint16_t MFRC630_ISO14443A_WUPA();

/*
 * Used to send WUPA and REQA. This actually sends WUPA and REQA and returns the
 * response byte.
 * INPUT: const uint8_t instruction - 
 * OUTPUT: uint16_t - The Answer to request A byte (ATQA), or zero in case of no
 *          answer.
 */
uint16_t MFRC630_ISO14443A_WUPA_REQA(const uint8_t instruction);

/*
 * Performs the SELECT procedure to discover a card's UID. This performs the
 * SELECT procedure as explained in ISO 14443A, this determines the UID of the
 * card, if multiple cards are present, a collision will occur, which is handled
 * according to the norm. This collision handling is explained quite complex in
 * the norm, but conceptually it is not all that complex:
 * - The cascade level can be seen as a prefix to ensure both the PICC and PCD
 *   are working on identifying the same part of the UID.
 * - The entire anti-collision scheme is more of a binary search, the PICC sends
 *   the CASCADE level prefix, then the NVB byte, this field determines how many
 *   bits of the UID will follow, this allows the PICC's to listen to this and
 *   respond if their UID's match these first bits with the UID that is
 *   transmitted. After this all PICC's (that have matched the UID bits already
 *   sent) respond with the remainder of their UIDS. This results in either a
 *   complete UID, or in case two PICC's share a few bits but then differ a bit,
 *   a collision occurs on this bit. This collision is detected by the PCD, at
 *   which point it can chose either to pursue the PICC(s) that has a 0b1 at
 *   that position, or pursue the 0b0 at that position. The ISO norm states: A
 *   typical implementation adds a (1)b. I use the bit value that's in the
 *   pointer at the same position as the collision, or at least for the first
 *   cascade level that works, after that it's off by a byte because of the
 *   cascade tag, see the actual implementation. 
 * INPUT: uint8_t uid - The UID of the card will be stored into this array. This
 *         array is also used to determine the choice between an 0b1 or 0b0 when
 *         a collision occurs. The bit that's in `uid` at the collision position
 *         is selected.
 *        uint8_t sak - The last SAK byte received during the SELECT procedure
 *         is placed here, this often holds information about the type of card.
 * OUTPUT: uint8_t - The length of the UID in bytes (4, 7, 10), or 0 in case of
 *          failure.
 */
uint8_t MFRC630_ISO14443A_Select(uint8_t* uid, uint8_t* sak);

// ---------------------------------------------------------------------------
// MIFARE
// ---------------------------------------------------------------------------
/*! \defgroup mifare MIFARE
    \brief Functions to interact with MIFARE RFID tags / cards.
    MIFARE cards have memory blocks of 16 bytes, read and write operations are always done one entire block of 16 bytes
    at a time.
  @{
*


/*! \brief Perform a MIFARE authentication procedure.
    This function is a higher-level wrapper around the MF authenticate command.
    internally it calls `mfrc630_cmd_auth()` which is described above, the result of the
    authentication is checked to identify whether it appears to have succeeded.
    The key must be loaded into the key buffer, by one of the following functions:
    \code
         void mfrc630_cmd_load_key_E2(uint8_t key_nr);
         void mfrc630_cmd_load_key(uint8_t* key);
    \endcode
    Once authenticated, the authentication MUST be stopped manually by calling
    the `mfrc630_MF_deauth()` function or otherwires disabling the Crypto1 ON bit
    in the status register.
    \param [in] key_type The MIFARE key A or B ( `#MFRC630_MF_AUTH_KEY_A` = 0x60 or `#MFRC630_MF_AUTH_KEY_B` = 0x61) to
                use.
    \param [in] block The block to authenticate.
    \param [in] uid The authentication procedure required the first four bytes of the card's UID to authenticate.
    \return 0 in case of failure, nonzero in case of success.
*
uint8_t mfrc630_MF_auth(const uint8_t* uid, uint8_t key_type, uint8_t block);

/*! \brief Disables MIFARE authentication.
  Disable the Crypto1 bit from the status register to disable encryption. This bit will never change to 0 by itself, so
  the register must be overwritten by the MCU.
*
void mfrc630_MF_deauth();

/*! \brief Read a block of memory from an authenticated card.
    Try to read a block of memory from the card with the appropriate timeouts and error checking.
    \param [in] block_address The block to read.
    \param [out] dest The array in which to write the 16 bytes read from the card.
    \return 0 for failure, otherwise the number of bytes received.
*
uint8_t mfrc630_MF_read_block(uint8_t block_address, uint8_t* dest);

/*! \brief Write a block of memory to an authenticated card.
    Try to write a block of memory from the card with the appropriate timeouts and error checking.
    \param [in] block_address The block to write to.
    \param [in] source The array containing the 16 bytes to be written to this block.
    \return 0 for failure, nonzero means success.
*
uint8_t mfrc630_MF_write_block(uint8_t block_address, const uint8_t* source);


/*! \brief An example to read the first four blocks.
  Reading from a MIFARE card has the following steps, which are implemented in this function.
  Load the correct register settings with:
    \code
      mfrc630_AN1102_recommended_registers(MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER);
    \endcode
  The normal procedure to read a card would be to wake them up using:
    \code
      mfrc630_iso14443a_REQA();
    \endcode
  If there is a response, start the SELECT procedure with 
    \code
      uint8_t uid_len = mfrc630_iso14443a_select(uid, &sak);
    \endcode
  If uid_len is nonzero, we have found a card and UID. We can attempt to
  authenticate with it, for example with the default key:
    
    \code
      uint8_t FFkey[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
      mfrc630_cmd_load_key(FFkey); // load into the key buffer
    \endcode
  Then attempt to authenticate block 0 using this FFkey as KEY_A type:
    \code
      mfrc630_MF_auth(uid, MFRC630_MF_AUTH_KEY_A, 0)
    \endcode
  Finally, if that succeeds, we may use:
    \code
      len = mfrc630_MF_read_block(0, readbuf)
    \endcode
  To read block 0 from the card. After which we call:
    \code
      mfrc630_MF_deauth()
    \endcode
  To disable the currently enabled encryption process.
*
void mfrc630_MF_example_dump();
//!  @}
*/

#ifdef	__cplusplus
}
#endif

#endif	/* MFRC630_H_ */

