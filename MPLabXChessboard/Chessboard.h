/* 
 * File:   Chessboard.h
 * Author: aunger
 *
 * Created on December 7, 2016, 5:44 PM
 */

#ifndef CHESSBOARD_H_
#define	CHESSBOARD_H_

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * HIGH and LOW are easier on the eyes than 1 and 0.
 */
#define HIGH 1
#define LOW 0

// x = target variable
#define BITMASK_SET(x, mask) ((x) |= (mask)) // set bit
#define BITMASK_CLEAR(x, mask) ((x) &= (~(mask))) // clear bit
#define BITMASK_FLIP(x, mask) ((x) ^= (mask)) // toggle bit
#define BITMASK_CHECK(x, mask) (((x) & (mask)) == (mask)) // check bit
#define BITMASK_MASK(x, mask) ((x) &= (mask)) //
    
#ifdef	__cplusplus
}
#endif

#endif	/* CHESSBOARD_H_ */

