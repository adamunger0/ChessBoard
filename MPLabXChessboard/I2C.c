/*
 * NAME: I2C.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines functions to read/write to/from arbitrary
 * devices that may be connected to either of the MSSP modules in the
 * PIC18LF46K22.
 */

#include <xc.h> // register definitions
#include "I2C.h" // modules

/*
 * Convenient bitmasking routines.
 * x = target variable
 */
#define BITMASK_SET(x, mask) ((x) |= (mask))
#define BITMASK_CLEAR(x, mask) ((x) &= (~(mask)))
#define BITMASK_FLIP(x, mask) ((x) ^= (mask))
#define BITMASK_CHECK(x, mask) (((x) & (mask)) == (mask))
#define BITMASK_MASK(x, mask) ((x) &= (mask))

/*
 * This will initialize both of the I2C modules on the PIC micro to 400kHz.
 * INPUT: none
 * OUTPUT: none
 */
void initI2C() {
    // 0x27 = 400kHz, 0x12 = 842kHz, 0xF = 1MHz, 0x9 = 1.6MHz, 0x7 = 2MHz, 0x5 = 2.6MHz, 0x4 = 3.2MHz, 0x3 = 4MHz
    SSP1ADD = 0xF; 
    SSP1STATbits.SMP = 1; // slew rate disabled (=1) for fast operation
    SSP1CON1bits.SSPM = 0b1000; // Enable I2C Master mode
    SSP1CON2 = 0; // Reset MSSP status Control Register
    SSP1CON1bits.SSPEN = 1; // Enable SDA and SCL

    // Init the MPL3115A2
    //SSP2ADD = 0x27; // 0x27 = 400kHz, 0xF = 1MHz, 0x7 = 2MHz
    //SSP2STATbits.SMP = 1; // slew rate disabled for fast operation
    //SSP2CON1bits.SSPM = 0b1000; // Enable I2C Master mode
    //SSP2CON2 = 0; // Reset MSSP status Control Register
    //SSP2CON1bits.SSPEN = 1; // Enable SDA and SCL

    // Ensure both interrupt flags are low
    //PIR3bits.SSP2IF = 0; // Clear MSSP Interrupt Flag
    PIR1bits.SSP1IF = 0; // Clear MSSP Interrupt Flag
}

/*
 * This will block until the given I2C module is idle. See pg. 259, 260, 261,
 * and 262 of the PIC manual for further clarification.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: none
 */
void i2cIdle(char module) {
    // wait until I2C bus and status idle (i.e. ACKEN, RCEN, PEN, RSEN, SEN)
    if (MSSP1 == module)
        while (BITMASK_CHECK(SSP1CON2, 0x1F) || SSP1STATbits.R_nW);
    else if (MSSP2 == module)
        while (BITMASK_CHECK(SSP2CON2, 0x1F) || SSP2STATbits.R_nW);
}

/*
 * Wait for the I2C interrupt flag to be set. This will happen at significant
 * times during the I2C TX/RX procedure.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: none
 */
static void i2cWaitInterrupt(char module) {
    if (MSSP1 == module) {
        while(!PIR1bits.SSP1IF);
        PIR1bits.SSP1IF = 0;
    }
    else if (MSSP2 == module) {
        while(!PIR3bits.SSP2IF);
        PIR3bits.SSP2IF = 0;
    }
}

/*
 * Send a start condition to the I2C bus.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: none
 */
void i2cStart(char module) {
    // Ensure the I2C module is idle
    i2cIdle(module);

    if (MSSP1 == module) SSP1CON2bits.SEN = 1;
    else if (MSSP2 == module) SSP2CON2bits.SEN = 1;

    i2cWaitInterrupt(module);
}

/*
 * Send a repeated start condition over the I2C bus.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: none
 */
void i2cRepStart(char module) {
    // Ensure the I2C module is idle
    i2cIdle(module);

    if (MSSP1 == module) SSP1CON2bits.RSEN = 1;
    else if (MSSP2 == module) SSP2CON2bits.RSEN = 1;

    i2cWaitInterrupt(module);
}

/*
 * Send a stop condition over the I2C bus.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: none
 */
void i2cStop(char module) {
    // Ensure the I2C module is idle
    i2cIdle(module);

    if (MSSP1 == module) SSP1CON2bits.PEN = 1;
    else if (MSSP2 == module) SSP2CON2bits.PEN = 1;

    i2cWaitInterrupt(module);
}

/*
 * Get the ack bit that was set by the slave.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: char - status of ack/nack
 */
char i2cSlaveAck(char module) {
    // Return: 1 = Acknowledge was not received from slave
    //         0 = Acknowledge was received from slave
    if (MSSP1 == module) return SSP1CON2bits.ACKSTAT;
    else if (MSSP2 == module) return SSP2CON2bits.ACKSTAT;
    
    return 0; // to keep the compiler happy...
}

/*
 * Write 8-bits to the I2C port. Assumes that i2cStart() has already been called
 * and that a device is waiting for this message. If data is an address, ensure
 * that the R/W bit is added to the byte.
 * INPUT: char data - data to be written to the SDA line
 * OUTPUT: data written - no return
 */
void i2cWrite(char module, char data) {
    // Ensure the I2C module is idle
    i2cIdle(module);

    if (MSSP1 == module) {
        // Send the Data to I2C Bus
        SSP1BUF = data;
        // Check for write collision
        if (SSP1CON1bits.WCOL) return;
    }
    else if (MSSP2 == module) {
        // Send the Data to I2C Bus
        SSP2BUF = data;
        // Check for write collision
        if (SSP2CON1bits.WCOL) return;
    }

    // Wait until write cycle is complete
    i2cWaitInterrupt(module);
}

/*
 * Send the ack condition given over the I2C bus. See I2C.h for definitions of
 * the ack/nack and MSSP modules.
 * INPUT: char module - MSSP1 or MSSP2
 *        char ack_type - ack/nack
 * OUTPUT: none
 */
void i2cMasterAck(char module, char ack_type) {
    if (MSSP1 == module) {
        // 1 = Not Acknowledge, 0 = Acknowledge
        SSP1CON2bits.ACKDT = ack_type;
        // Enable Acknowledge
        SSP1CON2bits.ACKEN = 1;
    }
    else if (MSSP2 == module) {
        // 1 = Not Acknowledge, 0 = Acknowledge
        SSP2CON2bits.ACKDT = ack_type;
        // Enable Acknowledge
        SSP2CON2bits.ACKEN = 1;
    }

    i2cWaitInterrupt(module);
}

/*
 * Read a byte of data from the I2C bus. The caller is responsible for calling
 * i2cStop() when finished.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: char - the byte that the slave sent.
 */
char i2cRead(char module) {
    // Ensure the I2C module is idle
    i2cIdle(module);

    // Enable Receive Mode master for 1 byte reception
    if (MSSP1 == module) SSP1CON2bits.RCEN = 1;
    else if (MSSP2 == module) SSP2CON2bits.RCEN = 1;

    // Wait until buffer is full
    i2cWaitInterrupt(module);

    // Buffer is full with slave's data - return it
    if (MSSP1 == module) return SSP1BUF;
    else if (MSSP2 == module) return SSP2BUF;

    // just to keep the compiler happy...
    return 0;
}