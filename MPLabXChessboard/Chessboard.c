/* 
 * File:   Chessboard.c
 * Author: aunger
 *
 * Created on December 7, 2016, 12:11 AM
 */

#include "PICSetup.h" // includes xc.h

//#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "Chessboard.h"
#include "PinMagic.h"
#include "I2C.h" // initI2C()
#include "MFRC630.h"

#define _XTAL_FREQ 64000000

void _delay_half_s();
void _delay_s();
void blinkStatus(bool success);
void coil1Enabled(bool status);
void coil2Enabled(bool status);
void initialTests();
void readWriteTests();
uint16_t readCoil1(uint16_t atqa);
uint16_t readCoil2(uint16_t atqa);
void tuneRxCct();

/*
 * 
 */
int main() {
    
    setupMicro();
    initI2C();    
    
    coil2Enabled(true);
    
    _delay_s();
    _delay_s();
    _delay_s();
    MFRC630_AN1102_RecommendedRegisters(MFRC630_PROTO_ISO14443A_106_MILLER_MANCHESTER);
    // MFRC630WriteReg(0x94, MFRC630_REG_RXTHRESHOLD); // adjust receive thresholds
    //MFRC630_AN1102_RecommendedRegisters(MFRC630_PROTO_ISO14443A_848_MILLER_BPSK);
    
    uint16_t atqa1 = 0; // default to off
    uint16_t atqa2 = 0; // default to off
    
    while(true) {
        
        blinkStatus(true);
        _delay_s();
        blinkStatus(false);
        _delay_s();
        
        //initialTests();
        //readWriteTests();
        
        //coil1Enabled(true);
        //_delay_s();
        //atqa1 = readCoil1(atqa1);
        //coil1Enabled(false);
        //_delay_s();
        
        //coil2Enabled(true);
        //atqa2 = readCoil2(atqa2);
        //coil2Enabled(false);
    }
    
    return (EXIT_SUCCESS);
}

void coil1Enabled(bool status) {
    PORTAbits.RA0 = !status;
}

void coil2Enabled(bool status) {
    PORTAbits.RA1 = !status;
}

uint16_t readCoil2(uint16_t atqa) {
    //uint16_t atqa = MFRC630_ISO14443A_WUPA();
    uint16_t atqa_1 = MFRC630_ISO14443A_REQA();
    if (atqa != atqa_1) {
        // if the response is non-zero then we got a response from the tag so
        // turn the LED on
        if (atqa_1 != 0) {
            PORTAbits.RA5 = 1;
            __delay_ms(1);
        }
        // no response - turn the LED off
        else {
            PORTAbits.RA4 = 0;
            __delay_ms(1);
        }
    }
    return atqa_1;
}

uint16_t readCoil1(uint16_t atqa) {
    //uint16_t atqa = MFRC630_ISO14443A_WUPA();
    uint16_t atqa_1 = MFRC630_ISO14443A_REQA();
    if (atqa != atqa_1) {
        // if the response is non-zero then we got a response from the tag so
        // turn the LED on
        if (atqa_1 != 0) {
            PORTAbits.RA4 = 1;
            __delay_ms(1);
        }
        // no response - turn the LED off
        else {
            PORTAbits.RA4 = 0;
            __delay_ms(1);
        }
    }
    return atqa_1;
}

void tuneRxCct() {
    uint16_t atqa = 0;
    for (int i = 0; i < 20; ++i) {
        coil1Enabled(true);
        atqa = readCoil1(atqa);
        coil1Enabled(false);
    }
}

void readWriteTests() {       
    uint16_t atqa = MFRC630_ISO14443A_REQA();
    blinkStatus(atqa != 0);
    _delay_s();
    _delay_s();
}

void initialTests() {
    // indicate start of tests
    PORTAbits.RA4 = 1;
    __delay_ms(1);
    PORTAbits.RA5 = 1;
    __delay_ms(1);

    // delay 3s to signal start of testing sequence
    _delay_s();
    _delay_s();
    _delay_s();

    PORTAbits.RA4 = 0;
    __delay_ms(1);
    PORTAbits.RA5 = 0;
    __delay_ms(1);
       
    _delay_s();
    
    // perform tests
        
    char data[10];
    char data1[10];
        
    // test 1
    MFRC630WriteReg(0x3E, MFRC630_REG_T1RELOADHI);
    blinkStatus(0x3E == MFRC630ReadReg(MFRC630_REG_T1RELOADHI));
        
    _delay_s();
        
    // test 2
    data[0] = 0x73;
    data[1] = 0x46;
    data1[0] = 0;
    data1[1] = 0;
    MFRC630WriteRegs(data, MFRC630_REG_T3RELOADHI, 2);
    data1[0] = MFRC630ReadReg(MFRC630_REG_T3RELOADHI);
    data1[1] = MFRC630ReadReg(MFRC630_REG_T3RELOADLO);
    blinkStatus(data[0] == data1[0] && data[1] == data1[1]);
        
    _delay_s();
       
    // test 3
    for (int i = 0; i < 10; ++i) data[i] = i + 0x32;
    for (int i = 0; i < 10; ++i) data1[i] = 0;
      
    MFRC630FlushFIFO();
    MFRC630WriteFIFO(data, 10);
    MFRC630ReadFIFO(data1, 10);
                
    blinkStatus(data[0] == data1[0]
            && data[1] == data1[1]
            && data[2] == data1[2]
            && data[3] == data1[3]
            && data[4] == data1[4]
            && data[5] == data1[5]
            && data[6] == data1[6]
            && data[7] == data1[7]
            && data[8] == data1[8]
            && data[9] == data1[9]
           );
        
    _delay_s();

    /* test 4
    for (int i = 0; i < 10; ++i) data1[i] = 0;
        
    MFRC630Cmd_WriteE2(0x47, 0x31);
    _delay_s();
    _delay_s();
    MFRC630Cmd_ReadE2(0x47, 1);
    _delay_s();
    _delay_s();
    MFRC630ReadFIFO(data1, 1);
    blinkStatus(data1[0] == 0x31);
       
    _delay_s();
        
    // test 5
    for (int i = 0; i < 10; ++i) data1[i] = 0;
        
    for (int i = 0; i < 10; ++i) {
        MFRC630Cmd_WriteE2(0xC0+i, 0x18+i);
        _delay_s();
        _delay_s();
    }
        
    MFRC630Cmd_ReadE2(0xC0, 10);
    _delay_s();
    _delay_s();
    MFRC630ReadFIFO(data1, 10);
        
    blinkStatus(data1[0] == 0x18
            && data1[1] == 0x19
            && data1[2] == 0x1A
            && data1[3] == 0x1B
            && data1[4] == 0x1C
            && data1[5] == 0x1D
            && data1[6] == 0x1E
            && data1[7] == 0x1F
            && data1[8] == 0x20
            && data1[9] == 0x21
           );
       
    _delay_s();
    */
}

void blinkStatus(bool success) {
    PORTAbits.RA4 = 0;
    __delay_ms(1);
    PORTAbits.RA5 = 0;
    __delay_ms(1);
    
    // success is 2 simultaneous blinks
    if (success) {
        PORTAbits.RA4 = 1;
        __delay_ms(1);
        PORTAbits.RA5 = 1;
        _delay_half_s();
        PORTAbits.RA4 = 0;
        __delay_ms(1);
        PORTAbits.RA5 = 0;
        _delay_half_s();
        PORTAbits.RA4 = 1;
        __delay_ms(1);
        PORTAbits.RA5 = 1;
        _delay_half_s();
        PORTAbits.RA4 = 0;
        __delay_ms(1);
        PORTAbits.RA5 = 0;
        _delay_half_s();
    }
    // fail is two alternating blinks
    else {
        PORTAbits.RA4 = 1;
        __delay_ms(1);
        PORTAbits.RA5 = 0;
        _delay_half_s();
        PORTAbits.RA4 = 0;
        __delay_ms(1);
        PORTAbits.RA5 = 1;
        _delay_half_s();
        PORTAbits.RA4 = 1;
        __delay_ms(1);
        PORTAbits.RA5 = 0;
        _delay_half_s();
        PORTAbits.RA4 = 0;
        __delay_ms(1);
        PORTAbits.RA5 = 1;
        _delay_half_s();
    }
    
    PORTAbits.RA4 = 0;
    __delay_ms(1);
    PORTAbits.RA5 = 0;
    __delay_ms(1);
}

void _delay_half_s() {
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    __delay_ms(10);
    
}

void _delay_s() {
    _delay_half_s();
    _delay_half_s();
}