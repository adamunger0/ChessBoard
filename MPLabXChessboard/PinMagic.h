/* 
 * File:   PinMagic.h
 * Author: aunger
 *
 * Created on December 7, 2016, 5:37 PM
 */

#ifndef PINMAGIC_H
#define	PINMAGIC_H

#ifdef	__cplusplus
extern "C" {
#endif

void setupMicro();

#ifdef	__cplusplus
}
#endif

#endif	/* PINMAGIC_H */

