difference() {
    // the main beam - 161mm x 5mm x 2mm
    cube([160.75,4.5,1.65]);
    // the notches in the beam - 2mm x 2.5mm x 2mm
    translate([58.3,2.4,0]) cube([2.4,2.7,3]);
    translate([108.3,2.4,0]) cube([2.4,2.7,3]);
    translate([158.3,2.4,0]) cube([2.7,2.7,3]);
}