difference() {
    // the main beam - 259mm x 5mm x 2mm
    cube([258.75,4.65,1.55]);
    // the notches in the beam - 2mm x 2.5mm x 2mm
    translate([58.3,2.4,0]) cube([2.4,2.7,3]);
    translate([108.3,2.4,0]) cube([2.4,2.7,3]);
    translate([158.3,2.4,0]) cube([2.4,2.7,3]);
    translate([208.3,2.4,0]) cube([2.4,2.7,3]);
}