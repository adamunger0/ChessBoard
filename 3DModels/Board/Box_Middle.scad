// This will create a tapered rectangle with the dimensions of l, w, and h. The scale factor will determine the amount of scaling of the tapered end. By default the scaled end is the top (+Z). Use sf=1 for no taper while sf=0.5 will cause the vertices to meet in the middle and the top will disppear. Using an sf<1 will cause the rectangle to take on an hourglass shape.
module taperedExtrusion(l,w,h,sf,center=false) {
    CubePoints = [
      [ 0,  0,  0 ],  // 0
      [ l,  0,  0 ],  // 1
      [ l,  w,  0 ],  // 2
      [ 0,  w,  0 ],  // 3

      [ l-(l*sf),  w-(w*sf),  h ],  // 4
      [ l*sf,  w-(w*sf),  h ],  // 5
      [ l*sf,  w*sf,  h ],  // 6
      [ l-(l*sf),  w*sf,  h ]]; // 7
  
    CubeFaces = [
      [0,1,2,3],  // bottom
      [4,5,1,0],  // front
      [7,6,5,4],  // top
      [5,6,2,1],  // right
      [6,7,3,2],  // back
      [7,4,0,3]]; // left

    if (center) {
        translate ([-1*l/2,-1*w/2,-1*h/2]) polyhedron(CubePoints, CubeFaces);
    }
    else {
        polyhedron(CubePoints, CubeFaces);
    }
}

// main body
union() {
    difference() {
        // primary beam
        cube([50.4,35.4,13.9]);
        // alignment notches
        translate([0,7.5,2.8]) cube([11,20.4,8.4]);
        translate([39.4,7.5,2.8]) cube([11,20.4,8.4]);
    }

    difference() {
        // top support ledge
        translate([0,35.4,0]) cube([50.4,11.6,22]);
        // notches for separator grid
        translate([23.4,41.8,10]) cube([3.2,5.2,12]);
        //translate([73.4,42,12]) cube([3.2,5,10]);
        //translate([123.4,42,12]) cube([3.2,5,10]);
        //translate([173.4,42,12]) cube([3.2,5,10]);
    }

    // outer lip
    translate([0,47,0]) cube([50.4,2.8,1.9]);

    difference() {
        // inner support ledge
        translate([0,35.4,22]) cube([50.4,4.9,1.9]);
        // notches for support grid
        translate([23.4,37.2,22]) cube([3.2,3.1,1.9]);
        //translate([123.4,37.4,22]) cube([3.2,3,2]);
    }
}

// for testing
//translate([0,-15,0]) {
//    union() {
//        cube([25,10,2], center=true);
//        translate([0,0,5.95]) taperedExtrusion(19.8,7.8,9.9,0.9,center=true);
//    }
//}

translate([10,-11]) cube([1,12,1]);
translate([40,-11]) cube([1,12,1]);
translate([-11,40]) cube([12,1,1]);
translate([49,40]) cube([12,1,1]);
difference() {
    translate([-15,-15,0]) cube([80,80,5]);
    translate([-10,-10,0]) cube([70,70,5]);
}
